import highlightOptions from '../../config/highlightOptions.js';

export default function (client) {
  client.on('interactionCreate', async (interaction) => {
    if (!interaction.isStringSelectMenu()) return;
  
    if (interaction.customId === 'highlight-enable-disable') {
      const highlightName = interaction.values[0];
      let found = false;
  
      for (const key in highlightOptions) {
        if (highlightOptions.hasOwnProperty(key) && key === highlightName) {
          highlightOptions[key].enabled = !highlightOptions[key].enabled;
          found = true;
          await interaction.reply({
            content: `Highlight '${highlightName}' has been ${
              highlightOptions[key].enabled ? 'enabled' : 'disabled'
            }.`,
            ephemeral: true,
          });
          break;
        }
      }
  
      if (!found) {
        await interaction.reply({
          content: `Highlight '${highlightName}' not found.`,
          ephemeral: true,
        });
      }
    } else if (interaction.customId === 'highlight-update-select') {
      const [optionName, attributeName] = interaction.values[0].split(':');
  
      await interaction.reply({
        content: `Please enter the new value for '${optionName} -> ${attributeName}':`,
        ephemeral: true,
      });
  
      const filter = (response) => response.author.id === interaction.user.id;
      const collector = interaction.channel.createMessageCollector({ filter, max: 1, time: 60000 });
  
      collector.on('collect', async (response) => {
  
        // Parse the new value and validate it
        const newValue = parseFloat(response.content);
        const minValue = 0;
        const maxValue = 1000;
  
        // If the value is not a number, or is outside the allowed range, send an error message
        if (isNaN(newValue) || newValue < minValue || newValue > maxValue) {
          await response.reply({
            content: `Please enter a valid number between ${minValue} and ${maxValue}.`,
            ephemeral: true,
          });
          return;
        }
  
        highlightOptions[optionName][attributeName] = newValue;
  
        // Send a confirmation message
        await interaction.followUp({
          content: `The value for '${optionName} -> ${attributeName}' has been updated to ${newValue}.`,
          ephemeral: true,
        });
  
        // Delete the user's response
        response.delete().catch(console.error);
      });
  
      collector.on('end', (collected, reason) => {
        if (reason === 'time') {
          interaction.followUp({
            content: 'No response received within 1 minute. Update canceled.',
            ephemeral: true,
          });
        }
      });
    }
  });
}
