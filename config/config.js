import fs from 'fs';

// Flag to indicate if we should fetch events from the blockchain in the stats.js script
export const FETCH_EVENTS = true;

// Battle contracts
export const CONTRACTS = 
[
  {
    type: "battle",
    contracts: [
      {
        address: "0xa015db2a753dc54a9702b895489747558e9764a6",
        abi: JSON.parse(fs.readFileSync('abis/0xa015db2a753dc54a9702b895489747558e9764a6.json', 'utf8')),
        endBlock: 76731394,
      },
      {
        address: "0x4eb66cbf70677a0e31c633d39752e98e151ac022",
        abi: JSON.parse(fs.readFileSync('abis/0x4eb66cbf70677a0e31c633d39752e98e151ac022.json', 'utf8')),
        endBlock: 82127324,
      },
      {
        address: "0xcfcd2371f19b31f9500f4581ac52ffbfaabbb18e",
        abi: JSON.parse(fs.readFileSync('abis/0xcfcd2371f19b31f9500f4581ac52ffbfaabbb18e.json', 'utf8')),
        endBlock: null,
      },
    ]
  },
  {
    type: "aov",
    contracts: [
      {
        address: "0x747910B74D2651A06563C3182838EAE4120F4277",
        abi: JSON.parse(fs.readFileSync('abis/AoV.json', 'utf8')),
        endBlock: null,
      },
    ]
  },
  {
    type: "magicDomainRegistrar",
    contracts: [
      {
        address: "0xdb8b3a07b93ec48336662a8efabaa8474a588a5d",
        abi: JSON.parse(fs.readFileSync('abis/MagicDomainRegistrar.json', 'utf8')),
        endBlock: null,
      },
    ]
  },
  {
    type: "treasureTag",
    contracts: [
      {
        address: "0xe50abe4756809b51b0041bb5ab12ec4c5c67af47",
        abi: JSON.parse(fs.readFileSync('abis/TreasureTag.json', 'utf8')),
        endBlock: null,
      },
    ]
  },
  {
    type: "transcendence",
    contracts: [
      {
        address: "0x5862ab89eddd299f9dd1f1daa4e34f6e9261c392",
        abi: JSON.parse(fs.readFileSync('abis/Transcendence.json', 'utf8')),
        endBlock: null,
      },
    ]
  },
];

// Statistics parameters
export const LOW_ODDS_THRESHOLD = 10; // Threshold for low odd trait battle wins
export const UPSET_WIN_THRESHOLD = 50; // Threshold for low odd trait battle wins

// Other parameters
export const CHUNK_SIZE = 2000; // Number of blocks to fetch logs in one request
export const FILES_CHUNK_SIZE = 100; // Number of chunks to save to disk in one file
export const MESSAGE_TIMEOUT = 2000; // Timeout between discord messages, in ms
export const TRAIT_NAMES = {
  2: 'strength',
  3: 'dexterity',
  4: 'constitution',
  5: 'intelligence',
  6: 'wisdom',
  7: 'charisma'
};
export const IPFS_GATEWAYS = [
  'https://cf-ipfs.com',
  'https://gateway.ipfs.io',
  'https://gateway.pinata.cloud',
  'https://ipfs.io',
  'https://ipfs.filebase.io',
  'https://cloudflare-ipfs.com',
];