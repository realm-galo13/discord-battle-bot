import { SlashCommandBuilder } from '@discordjs/builders';
import { monitorTransactions } from '../monitor.js';

const startmonitoring = {
  data: new SlashCommandBuilder()
    .setName('startmonitoring')
    .setDescription('Start monitoring events and send battle reports to the target channel'),
  execute: async (interaction, state, contract) => {
    if (!state.targetChannelId) {
      await interaction.reply('Target channel not set. Please set the target channel using /setchannel before starting monitoring.');
      return;
    }

    state.monitoring = true;
    monitorTransactions(contract, state);
    await interaction.reply('Monitoring started.');
  },
};

export default startmonitoring;