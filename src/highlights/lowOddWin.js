export function checkLowOddWinsHighlight(battle, options) {
  const winnerLowOddWins = battle.getWinnerLowOddWins(options.lowOddThreshold);

  if (winnerLowOddWins >= options.minLowOddWins) {
    return winnerLowOddWins;
  }
  return false;
}