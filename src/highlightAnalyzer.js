import { sendEmbed } from '../utils/messaging.js';
import { combineImages } from '../utils/combineImages.js';
import { getAdventurerImageIPFSHash } from '../utils/getAoVImage.js';
import { getAdventurersTreasureTags } from '../utils/getTreasureTags.js';
import { getFileUrlByName } from '../utils/helpers.js';

export async function analyzeHighlightCondition(state, battle, config) {
  try {
    const {
      checkFunction,
      options,
      color,
      title,
      description,
      adventurerFeatured,
      thumbnailImageUrl,
    } = config;
  
    const checkResult = checkFunction(battle, options);
    console.log("checkResult:", checkResult);
  
    // If the check function returns a truthy value, it means the highlight condition was met
    // and we should send the embed with this battle as the highlight
    if (checkResult) {
      let winner = battle.getWinner();
      let loser = battle.getLoser();
  
      let adventurerFeaturedId = null;
      let adventurer2FeaturedId = null;
      if (adventurerFeatured === 'winner') {
        adventurerFeaturedId = winner.id;
      } else if (adventurerFeatured === 'loser') {
        adventurerFeaturedId = loser.id;
      } else if (adventurerFeatured === 'attacker') {
        adventurerFeaturedId = battle.fightStartedData.attackerId;
      } else if (adventurerFeatured === 'opponent') {
        adventurerFeaturedId = battle.fightStartedData.opponentId;
      } else if (adventurerFeatured === 'both') {
        adventurerFeaturedId = battle.fightStartedData.attackerId;
        adventurer2FeaturedId = battle.fightStartedData.opponentId;
      }
  
      const winnerImageIPFSHash = await getAdventurerImageIPFSHash(state.provider, winner.id);
      const loserImageIPFSHash = await getAdventurerImageIPFSHash(state.provider, loser.id);
      const combinedImageBuffer = await combineImages(winnerImageIPFSHash, loserImageIPFSHash, getFileUrlByName('vsImage'));

      const treasureTags = await getAdventurersTreasureTags(state.provider, battle.fightStartedData.attackerId, battle.fightStartedData.opponentId);
  
      const embedContent = {
        fightId: battle.fightId,
        txHash: battle.transactionHash,
        color,
        title,
        description: adventurerFeatured === 'both' 
          ? description(adventurerFeaturedId, adventurer2FeaturedId, winner.id, checkResult)
          : description(adventurerFeaturedId, checkResult),
        thumbnailImage: thumbnailImageUrl,
        attackerScore: battle.getAttackerScore(),
        opponentScore: battle.getOpponentScore(),
        attackerId: battle.fightStartedData.attackerId,
        opponentId: battle.fightStartedData.opponentId,
        attackerTraitsWon: battle.getTraitsWonByAttacker(),
        opponentTraitsWon: battle.getTraitsWonByOpponent(),
        animaWonAttacker: battle.getAttackerAnima(),
        animaWonOpponent: battle.getOpponentAnima(),
        image: combinedImageBuffer,
        treasureTags
      };
  
      await sendEmbed(state.client, state.targetChannelId, embedContent);
      return true;
    }
  } catch (error) {
    console.error(error);
    return false;
  }
  return false;
}