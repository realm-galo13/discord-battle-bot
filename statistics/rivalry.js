import fs from 'fs';
import { mkdirp } from 'mkdirp';
import path from 'path';

export async function generateRivalryStats(eventsFightStarted, eventsOverallWinner) {
  const rivalryMap = new Map();
  const fightIdToParticipants = new Map();

  eventsFightStarted.forEach((event) => {
    const { fightId, attackerAddr, attackerId, opponentAddr, opponentId } = event;
    const attacker = `${attackerAddr}-${attackerId}`;
    const opponent = `${opponentAddr}-${opponentId}`;

    const key = [attacker, opponent].sort().join('/');
    if (!rivalryMap.has(key)) {
      rivalryMap.set(key, [key, 0, 0, 0]);
    }
    fightIdToParticipants.set(fightId, { attacker, opponent });
  });

  eventsOverallWinner.forEach((event) => {
    const { fightId, addr, adventureId } = event;
    const winner = `${addr}-${adventureId}`;

    const { attacker, opponent } = fightIdToParticipants.get(fightId);
    const key = [attacker, opponent].sort().join('/');
    const rivalry = rivalryMap.get(key);

    rivalry[1]++; // Increment number of battles
    if (winner === rivalry[0].split('/')[0]) {
      rivalry[2]++; // Increment first adventurer's wins
    } else {
      rivalry[3]++; // Increment second adventurer's wins
    }
  });

  return rivalryMap;
}

export async function saveRivalryToDisk(rivalryMap) {
  const filepath = path.join('statistics', 'results', 'rivalry', 'rivalry.json');

  const rivalryArray = Array.from(rivalryMap.values());
  const sortedRivalryArray = rivalryArray.sort((a, b) => b[1] - a[1]);

  await mkdirp(path.dirname(filepath));

  return new Promise((resolve, reject) => {
    fs.writeFile(filepath, JSON.stringify(sortedRivalryArray, null, 2), (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}