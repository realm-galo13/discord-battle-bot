import fs from 'fs';
import { mkdirp } from 'mkdirp';
import path from 'path';
import axios from 'axios';

export function createChunks(startBlock, endBlock, chunkSize) {
  const chunks = [];

  while (startBlock <= endBlock) {
    const end = Math.min(startBlock + chunkSize - 1, endBlock);
    chunks.push({ start: startBlock, end: end });
    startBlock += chunkSize;
  }

  return chunks;
}

export function writeToFile(filename, data) {
  return new Promise((resolve, reject) => {
    mkdirp.sync(filename.substring(0, filename.lastIndexOf('/')));
    fs.writeFile(filename, JSON.stringify(data, null, 2), (error) => {
      if (error) {
        reject(error);
      } else {
        resolve();
      }
    });
  });
}

export function appendToFile(filename, data) {
  fs.appendFile(filename, data, function (err) {
    if (err) throw err;
  });
}  

export function convertMapToFormattedStrings(map) {
  const formattedStrings = [...map.entries()].map(([key, value]) => {
    return [key, divideByDecimalFactor(value, 18)];
  });
  return new Map(formattedStrings);
}

export function divideByDecimalFactor(value, decimalFactor) {
  const stringValue = value.toString();
  const integerPart = stringValue.slice(0, -decimalFactor) || '0';
  const fractionalPart = stringValue.slice(-decimalFactor);
  return `${integerPart}.${fractionalPart}`;
}

function bigIntReplacer(key, value) {
  if (typeof value === 'bigint') {
    return value.toString();
  }
  return value;
}

export function sortMapByValue(map) {
  return new Map([...map.entries()].sort((a, b) => b[1] > a[1] ? 1 : b[1] < a[1] ? -1 : 0));
}

export function calculateAverageAdventurerAnimaWon (animaMap, battleCountMap) {
  const averageAnimaWon = new Map();
  for (const [key, anima] of animaMap.entries()) {
    const battleCount = battleCountMap.get(key);
    averageAnimaWon.set(key, { battleCount, averageAnima: (anima / battleCount) });
  }
  return new Map([...averageAnimaWon.entries()].sort((a, b) => b[1].averageAnima - a[1].averageAnima));
};

export function calculatePercentageOfBattlesWithAtLeastNUpsetWins(upsetWinsPerBattle, numberofBattles, n) {
  const battlesWithAtLeastNUpsetWins = Array.from(upsetWinsPerBattle.values()).filter((count) => count >= n).length;
  return (battlesWithAtLeastNUpsetWins / numberofBattles) * 100;
}

export async function saveParsedEventsToDisk(contractAddress, blockRange, eventsFightStarted, eventsContenderRoll, eventsOverallWinner) {
  const dataDir = 'data';
  const contractDataDir = path.join(dataDir, contractAddress);

  // Create the data directory if it doesn't exist
  await mkdirp(contractDataDir);

  const filename = `events_${blockRange.start}_${blockRange.end}.json`;
  const filepath = path.join(contractDataDir, filename);

  const dataToSave = {
    blockRange,
    eventsFightStarted,
    eventsContenderRoll,
    eventsOverallWinner,
  };

  return new Promise((resolve, reject) => {
    fs.writeFile(filepath, JSON.stringify(dataToSave, bigIntReplacer, 2), (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

export async function readSingleFile(filepath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, 'utf-8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(JSON.parse(data));
      }
    });
  });
}

export function logMemoryUsage() {
  const used = process.memoryUsage().heapUsed / 1024 / 1024;;
  const total = process.memoryUsage().heapTotal / 1024 / 1024;;
  const percentUsed = ((used / total) * 100).toFixed(2);
  console.log(`Memory usage: ${used} MB / ${total} MB (${percentUsed}%)`);
}

export async function getLatestBlockFromLogs(contractAddress) {
  const dataDir = path.join('data', contractAddress);
  try {
    await fs.promises.mkdir(dataDir, { recursive: true });
  } catch (err) {
    console.error(err);
  }
  const filenames = await fs.promises.readdir(dataDir);

  // Filter and sort filenames
  const sortedFilenames = filenames
    .filter(filename => filename.match(/events_\d+_\d+\.json/))
    .sort();

  const latestFile = sortedFilenames[sortedFilenames.length - 1];
  if (!latestFile) {
    return -1; // Return -1 if no matching files found
  }

  const match = latestFile.match(/events_\d+_(\d+)\.json/);
  const latestBlock = parseInt(match[1], 10);

  return latestBlock;
}

export async function getEarliestBlockFromLogs(contractAddress) {
  const dataDir = path.join('data', contractAddress);
  try {
    await fs.promises.mkdir(dataDir, { recursive: true });
  } catch (err) {
    console.error(err);
  }
  const filenames = await fs.promises.readdir(dataDir);

  // Filter and sort filenames
  const sortedFilenames = filenames
    .filter(filename => filename.match(/events_\d+_\d+\.json/))
    .sort();

  const earliestFile = sortedFilenames[0];
  if (!earliestFile) {
    return -1; // Return -1 if no matching files found
  }

  const [startBlock, endBlock] = earliestFile.match(/events_(\d+)_(\d+)\.json/).slice(1).map(Number);
  return startBlock;
}

export function getFilenameFromUrl(url) {
  const urlObj = new URL(url);
  const pathname = urlObj.pathname;
  const filename = path.basename(pathname);
  return filename;
}

export function getFileUrlByName(name) {
  const fileUrls = JSON.parse(fs.readFileSync('config/fileUrls.json', 'utf8'));
  const fileUrlObj = fileUrls.find(fileUrl => fileUrl.name === name);
  return fileUrlObj ? fileUrlObj.url : null;
}

export function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

export async function withRetries(fn, retries = 3, delay = 1000) {
  let lastError;

  for (let i = 0; i <= retries; i++) {
    try {
      return await fn();
    } catch (error) {
      lastError = error;
      await new Promise((resolve) => setTimeout(resolve, delay));
      delay *= 2;
    }
  }

  throw lastError;
}

export async function fetchDeploymentTransactionArbiscan(contractAddress) {
  const url = `https://api.arbiscan.io/api?module=account&action=txlist&address=${contractAddress}&sort=asc&page=1&offset=1&apikey=${process.env.ARBISCAN_API_KEY}`;
  const response = await axios.get(url);
  const transactions = response.data.result;

  if (transactions.length > 0) {
    return transactions[0];
  }

  throw new Error('No transactions found for the provided contract address');
}