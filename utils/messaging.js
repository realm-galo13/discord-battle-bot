import { EmbedBuilder, AttachmentBuilder } from 'discord.js';
import { MESSAGE_TIMEOUT } from "../config/config.js";

var isSendingMessages = false;
var messageQueue = [];

export async function sendMessage(client, targetChannelId, messageContent) {
  const targetChannel = client.channels.cache.get(targetChannelId);
  if (!targetChannel) {
    console.error(`Channel not found for ID: ${targetChannelId}`);
    return;
  }

  messageQueue.push({ type: 'text', content: messageContent });
  if (isSendingMessages) return;
  await processQueue(client, targetChannelId);
}

export async function sendEmbed(client, targetChannelId, embedContent) {
  const embedData = { type: 'embed', content: embedContent };
  messageQueue.push(embedData);

  if (isSendingMessages) return;

  await processQueue(client, targetChannelId);
}

async function processQueue(client, targetChannelId) {
  isSendingMessages = true;

  const targetChannel = client.channels.cache.get(targetChannelId);

  while (messageQueue.length > 0) {
    const messageData = messageQueue.shift();
    if (messageData.type === 'text') {
      await targetChannel.send(messageData.content);
    } else if (messageData.type === 'embed') {
      const { embedMessage, battleImage } = createEmbedMessage(client, messageData.content);
      await targetChannel.send({ embeds: [embedMessage], files: [battleImage] });
    }
    await sleep(MESSAGE_TIMEOUT);
  }

  isSendingMessages = false;
}

function createEmbedMessage(client, content) {
  try {
    const battleImage = new AttachmentBuilder(content.image, { name: "battleImage.webp" });

    const embedMessage = new EmbedBuilder()
    .setColor(content.color)
    .setTitle(`Battle Highlight: ${content.title}`)
    .setURL(`https://arbiscan.io/tx/${content.txHash}`)
    .setDescription(content.description)
    .setThumbnail(content.thumbnailImage)
    .addFields(
      { name: 'Battle #', value: `${content.fightId}`, inline: true },
      { name: '\u200b', value: '\u200b', inline: true },
      { name: 'Score', value: `${content.attackerScore} x ${content.opponentScore}`, inline: true },
      { name: 'Attacker', value: `#${content.attackerId}\n${content.treasureTags.attacker}`, inline: true },
      { name: 'Opponent', value: `#${content.opponentId}\n${content.treasureTags.opponent}`, inline: true },
      { name: 'Attacker winning traits', value: `${content.attackerTraitsWon}` },
      { name: 'Opponent winning traits', value: `${content.opponentTraitsWon}` },
      { name: 'Anima Attacker', value: `${parseFloat(content.animaWonAttacker).toFixed(2)}`, inline: true },
      { name: 'Anima Opponent', value: `${parseFloat(content.animaWonOpponent).toFixed(2)}`, inline: true }
    )
    .setImage('attachment://battleImage.webp')
    .setTimestamp()
    .setFooter({
      text: 'Powered by Realm Battle Highlights Bot',
      iconURL: client.user.displayAvatarURL(),
    });

    return { embedMessage, battleImage };
  } catch (error) {
    console.error(error);
  }
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}