export function checkFlawlessVictoryHighlight(battle, options) {
  const attackerScore = battle.getAttackerScore();

  // If the attacker's score is 6 or 0, a flawless victory has occurred
  if (attackerScore === 6 || attackerScore === 0) {
    return true;
  }

  return false;
}