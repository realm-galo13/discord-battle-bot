import { SlashCommandBuilder } from '@discordjs/builders';

const stopmonitoring = {
  data: new SlashCommandBuilder()
    .setName('stopmonitoring')
    .setDescription('Stop monitoring events and sending battle reports to the target channel'),
  execute: async (interaction, state) => {
    if (!state.monitoring) {
      await interaction.reply('Monitoring is not currently running.');
      return;
    }

    state.monitoring = false;
    await interaction.reply('Monitoring stopped.');
  },
};

export default stopmonitoring;