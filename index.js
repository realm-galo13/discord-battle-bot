import { ethers } from 'ethers';
import { Client, GatewayIntentBits } from 'discord.js';
import dotenv from 'dotenv';
import { CONTRACTS } from './config/config.js';
import readyEvent from './src/events/ready.js';
import commandInteractionEvent from './src/events/commandInteraction.js';
import selectMenuInteractionEvent from './src/events/selectMenuInteraction.js';

dotenv.config();

const token = process.env.DISCORD_BOT_TOKEN;
const provider = new ethers.JsonRpcProvider(process.env.PROVIDER_URL);
const contract = new ethers.Contract(CONTRACTS[CONTRACTS.length - 1].address, CONTRACTS[CONTRACTS.length - 1].abi, provider);

const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent
  ]
});

const state = {
  targetChannelId: '',
  client,
  provider,
  monitoring: false
};

readyEvent(client);
commandInteractionEvent(client, state, contract);
selectMenuInteractionEvent(client);

client.login(token);