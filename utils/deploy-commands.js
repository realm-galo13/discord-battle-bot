import { REST, Routes } from 'discord.js';
import { readdirSync } from 'fs';
import dotenv from 'dotenv';

dotenv.config();

const commands = [];
const commandFiles = readdirSync('src/commands').filter((file) => file.endsWith('.js'));

for (const file of commandFiles) {
  console.log(file);
  const command = await import(`../src/commands/${file}`);
  commands.push(command.default.data.toJSON());
}

// Construct and prepare an instance of the REST module
const rest = new REST().setToken(process.env.DISCORD_BOT_TOKEN);

(async () => {
  try {
    console.log('Started refreshing application (/) commands.');

    //Deploy to a single guild (use in dev)
    /*await rest.put(
      Routes.applicationGuildCommands(process.env.DISCORD_APP_CLIENT_ID, process.env.DISCORD_GUILD_ID),
      { body: commands }
    );*/
    //Deploy to all guilds
    await rest.put(
      Routes.applicationCommands(process.env.DISCORD_APP_CLIENT_ID),
      { body: commands }
    );

    console.log('Successfully reloaded application (/) commands.');
  } catch (error) {
    console.error(error);
  }
})();