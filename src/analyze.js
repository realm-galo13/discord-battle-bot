import { getFileUrlByName, shuffleArray } from '../utils/helpers.js';
import { analyzeHighlightCondition } from './highlightAnalyzer.js';
import { checkWinStreakHighlight } from './highlights/winStreak.js';
import { checkLossStreakHighlight } from './highlights/lossStreak.js';
import { checkTopPercentAnimaAttackersHighlight } from './highlights/topPercentAnimaAttackers.js';
import { checkTopPercentAnimaOpponentsHighlight } from './highlights/topPercentAnimaOpponents.js';
import { checkLowOddWinsHighlight } from './highlights/lowOddWin.js';
import { checkFlawlessVictoryHighlight } from './highlights/flawlessVictory.js';
import { checkUpsetWinsHighlight } from './highlights/upsetWin.js';
import { checkRivalryHighlight } from './highlights/rivalry.js';
import { checkTraitMasterHighlight } from './highlights/traitMaster.js';
import { COLORS } from '../config/colors.js';
import highlightOptions from '../config/highlightOptions.js';

let battleCounter = 0;
let highlightCounter = 0;

const analyses = [
  {
    checkFunction: checkWinStreakHighlight,
    options: highlightOptions.winStreak,
    color: COLORS.LIGHT_GREEN,
    title: 'Win Streak',
    description: (winnerAdventurerId, newStreak) => `**Whoa!** Adventurer #**${winnerAdventurerId}** is on fire! 🔥 They've just secured another win, making it a solid **${newStreak} win streak**! Keep slaying, adventurer! 💪`,
    adventurerFeatured: 'winner',
    thumbnailImageUrl: getFileUrlByName('winStreak'),
  },
  {
    checkFunction: checkLossStreakHighlight,
    options: highlightOptions.lossStreak,
    color: COLORS.RED,
    title: 'Loss Streak',
    description: (loserAdventurerId, newStreak) => `**Hang in there!** Adventurer #**${loserAdventurerId}** has hit a **${newStreak}-loss streak**. 💪👊 Keep pushing forward, and better luck in your next battles. 🍀`,
    adventurerFeatured: 'loser',
    thumbnailImageUrl: getFileUrlByName('lossStreak'),
  },
  {
    checkFunction: checkTopPercentAnimaAttackersHighlight,
    options: highlightOptions.topPercentAnimaAttackers,
    color: COLORS.YELLOW,
    title: `Top ${highlightOptions.topPercentAnimaAttackers.topPercent}% Anima Attacker`,
    description: (attackerAdventurerId, animaWon) => `**Impressive!** Adventurer #**${attackerAdventurerId}** has shown remarkable skill in battle, earning **${parseFloat(animaWon).toFixed(2)} anima** as the attacker. This puts them in the **top ${highlightOptions.topPercentAnimaAttackers.topPercent}%** of anima earners! Keep up the great work! 🌟`,
    adventurerFeatured: 'attacker',
    thumbnailImageUrl: getFileUrlByName('topPercentAnimaAttackers'),
  },
  {
    checkFunction: checkTopPercentAnimaOpponentsHighlight,
    options: highlightOptions.topPercentAnimaOpponents,
    color: COLORS.YELLOW,
    title: `Top ${highlightOptions.topPercentAnimaOpponents.topPercent}% Anima Opponent`,
    description: (opponentAdventurerId, animaWon) => `**Remarkable!** Adventurer #**${opponentAdventurerId}** has demonstrated exceptional prowess in battle, earning **${parseFloat(animaWon).toFixed(2)} anima** as the opponent. This places them in the **top ${highlightOptions.topPercentAnimaOpponents.topPercent}%** of anima earners as the opponent! Keep up the fantastic performance! 🌟`,
    adventurerFeatured: 'opponent',
    thumbnailImageUrl: getFileUrlByName('topPercentAnimaAttackers'),
  },
  {
    checkFunction: checkLowOddWinsHighlight,
    options: highlightOptions.lowOddWins,
    color: COLORS.DARK_GREEN,
    title: `Low Odd Win`,
    description: (winnerAdventurerId, lowOddWins) => `**Incredible!** Adventurer #**${winnerAdventurerId}** has defied the odds, winning **${lowOddWins}** trait battles with less than **${highlightOptions.lowOddWins.lowOddThreshold}%** chance of victory. A true display of skill and luck! They say lightning never strikes twice, but this adventurer has proven otherwise! ⚡🎲`,
    adventurerFeatured: 'winner',
    thumbnailImageUrl: getFileUrlByName('lowOddWin'),
  },
  {
    checkFunction: checkFlawlessVictoryHighlight,
    options: highlightOptions.flawlessVictory,
    color: COLORS.BLUE,
    title: 'Flawless Victory',
    description: (winnerAdventurerId) => `**Unstoppable!** Adventurer #**${winnerAdventurerId}** has achieved a flawless victory, winning all **6** trait battles! A truly dominant performance! 🏆`,
    adventurerFeatured: 'winner',
    thumbnailImageUrl: getFileUrlByName('flawlessVictory'),
  },
  {
    checkFunction: checkUpsetWinsHighlight,
    options: highlightOptions.upsetWins,
    color: COLORS.PINK,
    title: `Upset Win`,
    description: (winnerAdventurerId, upsetWins) => `**Amazing!** Adventurer #**${winnerAdventurerId}** has shown that underdogs can prevail, achieving **${upsetWins}** upset wins in trait battles. This adventurer has shown that anything is possible with determination and a little bit of luck! 🍀🎲`,
    adventurerFeatured: 'winner',
    thumbnailImageUrl: getFileUrlByName('upsetWin'),
  },
  {
    checkFunction: checkRivalryHighlight,
    options: highlightOptions.rivalry,
    color: COLORS.ORANGE,
    title: 'Rivalry Battle',
    description: (attackerAdventurerId, opponentAdventurerId, winnerAdventurerId, rivalryEntry) => {
      const totalBattles = rivalryEntry[1];
      const attackerWins = rivalryEntry[2];
      const opponentWins = rivalryEntry[3];
      return `**A fierce rivalry continues!** Adventurer #**${attackerAdventurerId}** and Adventurer #**${opponentAdventurerId}** have met in battle **${totalBattles} times**. So far, #**${attackerAdventurerId}** has won **${attackerWins} times**, while #**${opponentAdventurerId}** has won **${opponentWins} times**. Oh, this time? #**${winnerAdventurerId}** takes it home! 🔥`;
    },
    adventurerFeatured: 'both',
    thumbnailImageUrl: getFileUrlByName('rivalry'),
  },
  {
    checkFunction: checkTraitMasterHighlight,
    options: highlightOptions.traitMaster,
    color: COLORS.BLACK,
    title: 'Trait Master',
    description: (winnerAdventurerId, traitMasterHighlight) => {
      switch (traitMasterHighlight.trait.name) {
        case 'strength':
          return `**A mighty champion has emerged!** Adventurer #**${winnerAdventurerId}** proved their unrivaled mastery of the **${traitMasterHighlight.trait.name}** trait, crushing their foe with sheer force. Their record of **${traitMasterHighlight.trait.wins} wins** and **${traitMasterHighlight.trait.losses} losses** in this trait showcases their extraordinary prowess! 💪🔥`;
    
        case 'dexterity':
          return `**Behold the agile victor!** Adventurer #**${winnerAdventurerId}** displayed exceptional mastery of the **${traitMasterHighlight.trait.name}** trait, outmaneuvering their opponent with ease. With a remarkable **${traitMasterHighlight.trait.wins} wins** and only **${traitMasterHighlight.trait.losses} losses** in this trait, they dance circles around the competition! 🏃‍♂️💨`;
    
        case 'constitution':
          return `**An unbreakable fortress stands tall!** Adventurer #**${winnerAdventurerId}** demonstrated their impenetrable mastery of the **${traitMasterHighlight.trait.name}** trait, enduring every attack their enemy could muster. Their astounding **${traitMasterHighlight.trait.wins} wins** and a mere **${traitMasterHighlight.trait.losses} losses** in this trait cements them as a true bastion of resilience! 🛡️⚔️`;
    
        case 'intelligence':
          return `**A brilliant mind claims victory!** Adventurer #**${winnerAdventurerId}** showed off their keen mastery of the **${traitMasterHighlight.trait.name}** trait, outsmarting their adversary at every turn. With a splendid record of **${traitMasterHighlight.trait.wins} wins** and only **${traitMasterHighlight.trait.losses} losses** in this trait, they are the embodiment of intellectual supremacy! 🧠🎓`;
    
        case 'wisdom':
          return `**A sage emerges triumphant!** Adventurer #**${winnerAdventurerId}** revealed their profound mastery of the **${traitMasterHighlight.trait.name}** trait, making enlightened decisions that led them to victory. Boasting an impressive **${traitMasterHighlight.trait.wins} wins** and a mere **${traitMasterHighlight.trait.losses} losses** in this trait, they truly possess the wisdom of the ages! 📚🔮`;
    
        case 'charisma':
          return `**A charismatic conqueror shines!** Adventurer #**${winnerAdventurerId}** captivated all with their masterful display of the **${traitMasterHighlight.trait.name}** trait, charming their way to success. Their dazzling record of **${traitMasterHighlight.trait.wins} wins** and only **${traitMasterHighlight.trait.losses} losses** in this trait proves they can charm even the most formidable foes! 🎭🌟`;
    
        default:
          return `**An exceptional master prevails!** Adventurer #**${winnerAdventurerId}** demonstrated their remarkable mastery of the **${traitMasterHighlight.trait.name}** trait, securing victory in this intense battle.`;
    }},
    adventurerFeatured: 'winner',
    thumbnailImageUrl: getFileUrlByName('traitMaster'),
  }
];

export async function analyzeCompletedBattle(state, battle) {
  battleCounter++;

  const enabledAnalyses = analyses.filter(analysis => analysis.options.enabled);
  const shuffledAnalyses = shuffleArray(enabledAnalyses);

  for (const analysis of shuffledAnalyses) {
    const result = await analyzeHighlightCondition(state, battle, analysis);
    if (result) {
      highlightCounter++;
      console.log(`Analyzed ${battleCounter} battles.`);
      console.log(`${highlightCounter} highlights found.`);  
      break;
    }
  }
}