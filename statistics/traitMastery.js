// traitMastery.js
import fs from 'fs';
import { mkdirp } from 'mkdirp';
import path from 'path';
import { TRAIT_NAMES } from '../config/config.js';

export async function generateTraitMasteryStats(eventsFightStarted, eventsContenderRoll) {
  const adventurerMap = new Map();
  const fightIdToParticipants = new Map();

  eventsFightStarted.forEach((event) => {
    const { fightId, attackerAddr, attackerId, opponentAddr, opponentId } = event;
    const attacker = `${attackerAddr}-${attackerId}`;
    const opponent = `${opponentAddr}-${opponentId}`;

    [attacker, opponent].forEach((participant) => {
      if (!adventurerMap.has(participant)) {
        const traitsStats = {};
        for (const key in TRAIT_NAMES) {
          if (TRAIT_NAMES.hasOwnProperty(key)) {
            const name = TRAIT_NAMES[key];
            traitsStats[name] = { wins: 0, losses: 0 };
          }
        }

        adventurerMap.set(participant, {
          adventurer: participant,
          battles: 0,
          traits: traitsStats,
        });
      }
    });

    fightIdToParticipants.set(fightId, { attacker, opponent });
  });

  eventsContenderRoll.forEach((event) => {
    const { fightId, winnerAddr, winnerId, traitId } = event;
    const winner = `${winnerAddr}-${winnerId}`;

    const { attacker, opponent } = fightIdToParticipants.get(fightId);
    const loser = attacker === winner ? opponent : attacker;

    const trait = TRAIT_NAMES[traitId];

    adventurerMap.get(winner).battles++;
    adventurerMap.get(loser).battles++;
    adventurerMap.get(winner).traits[trait].wins++;
    adventurerMap.get(loser).traits[trait].losses++;
  });

  return adventurerMap;
}

export async function saveTraitMasteryToDisk(adventurerMap) {
  for (const key in TRAIT_NAMES) {
    const sortedAdventurers = sortAdventurersByTraitWinLossRatio(adventurerMap, TRAIT_NAMES[key]);
    const filteredAdventurers = filterAdventurerTraitStats(sortedAdventurers, TRAIT_NAMES[key]);

    const filepath = path.join('statistics', 'results', 'traitMastery', `${TRAIT_NAMES[key]}Mastery.json`);

    await mkdirp(path.dirname(filepath));

    await new Promise((resolve, reject) => {
      fs.writeFile(filepath, JSON.stringify(filteredAdventurers, null, 2), (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
}

function sortAdventurersByTraitWinLossRatio(adventurerMap, trait) {
  const sortedAdventurers = Array.from(adventurerMap.values())
    .sort((a, b) => {
      const aTraitStats = a.traits[trait];
      const bTraitStats = b.traits[trait];
      const aRatio = aTraitStats.wins / (aTraitStats.losses || 1);
      const bRatio = bTraitStats.wins / (bTraitStats.losses || 1);
      return bRatio - aRatio;
  });

  return sortedAdventurers;
}

function filterAdventurerTraitStats(adventurers, targetTrait) {
  return adventurers.map((adventurer) => ({
    adventurer: adventurer.adventurer,
    battles: adventurer.battles / 6,
    [targetTrait]: adventurer.traits[targetTrait],
  }));
}