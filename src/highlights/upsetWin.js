export function checkUpsetWinsHighlight(battle, options) {
  const winnerUpsetWins = battle.getWinnerUpsetTraitWins();
  if (winnerUpsetWins >= options.minUpsetWins) {
    return winnerUpsetWins;
  }
  return false;
}