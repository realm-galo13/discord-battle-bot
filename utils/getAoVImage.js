import { ethers } from "ethers";
import { CONTRACTS } from "../config/config.js";
import { withRetries } from "./helpers.js";

export async function getAdventurerImageIPFSHash(provider, adventurerId) {
  const aovContractGroup = CONTRACTS.find((group) => group.type === 'aov');
  const aovContractDetails = aovContractGroup.contracts[0];
  const contract = new ethers.Contract(aovContractDetails.address, aovContractDetails.abi, provider);

  const tokenURI = await withRetries(async () => {
    return await contract.tokenURI(adventurerId);
  });

  const tokenData = JSON.parse(tokenURI.substring(tokenURI.indexOf('{')));
  const imageUrl = tokenData.image;
  const ipfsHash = imageUrl.replace(/https?:\/\/[^/]+\/ipfs\//, '');
  return ipfsHash;
}