import { SlashCommandBuilder } from '@discordjs/builders';
import { ChannelType } from 'discord.js';

const setchannel = {
  data: new SlashCommandBuilder()
    .setName('setchannel')
    .setDescription('Set the channel for receiving battle highlight reports')
    .addChannelOption((option) =>
      option
        .setName('channel')
        .setDescription('The channel to set for receiving battle highlight reports')
        .setRequired(true)
    ),
  execute: async (interaction, state) => {
    const channel = interaction.options.getChannel('channel');
    if (channel.type !== ChannelType.GuildText && channel.type !== ChannelType.GuildAnnouncement) {
      await interaction.reply('Please select a text channel.');
      return false;
    }

    state.targetChannelId = channel.id;
    await interaction.reply(`Channel set to: ${channel.name}`);
    return true;
  },
};

export default setchannel;