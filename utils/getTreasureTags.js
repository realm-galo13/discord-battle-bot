import { ethers } from "ethers";
import { CONTRACTS } from "../config/config.js";
import { withRetries } from "./helpers.js";

export async function getAdventurersTreasureTags(provider, adventurerId1, adventurerId2) {
  const aovContractGroup = CONTRACTS.find((group) => group.type === 'aov');
  const aovContractDetails = aovContractGroup.contracts[0];
  const contractAoV = new ethers.Contract(aovContractDetails.address, aovContractDetails.abi, provider);

  const treasureTagContractGroup = CONTRACTS.find((group) => group.type === 'treasureTag');
  const magicDomainRegistrarContractGroup = CONTRACTS.find((group) => group.type === 'magicDomainRegistrar');
  const treasureTagContractDetails = treasureTagContractGroup.contracts[0];
  const magicDomainRegistrarContractDetails = magicDomainRegistrarContractGroup.contracts[0];
  const contractTag = new ethers.Contract(treasureTagContractDetails.address, magicDomainRegistrarContractDetails.abi, provider);  

  const ownerOfAdventurer1 = await withRetries(async () => {
    return await contractAoV.ownerOf(adventurerId1);
  });
  const ownerOfAdventurer2 = await withRetries(async () => {
    return await contractAoV.ownerOf(adventurerId2);
  });

  const ownerSubdomainToken1 = await withRetries(async () => {
    return await contractTag.userToOwnedSubdomainToken(ownerOfAdventurer1);
  });
  const ownerSubdomainToken2 = await withRetries(async () => {
    return await contractTag.userToOwnedSubdomainToken(ownerOfAdventurer2);
  });

  const treasureTag1 = await withRetries(async () => {
    return await contractTag.tokenMetadata(ownerSubdomainToken1);
  });
  const treasureTag2 = await withRetries(async () => {
    return await contractTag.tokenMetadata(ownerSubdomainToken2);
  });

  return {
    attacker: treasureTag1 ? `✨${treasureTag1.name}#${treasureTag1.discriminant}` : 'No Treasure Tag',
    opponent: treasureTag2 ? `✨${treasureTag2.name}#${treasureTag2.discriminant}` : 'No Treasure Tag',
  };
}