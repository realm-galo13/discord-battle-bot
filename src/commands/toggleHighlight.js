import { SlashCommandBuilder, ActionRowBuilder, StringSelectMenuBuilder, StringSelectMenuOptionBuilder } from '@discordjs/builders';
import highlightOptions from '../../config/highlightOptions.js';

const togglehighlight = {
  data: new SlashCommandBuilder()
    .setName('togglehighlight')
    .setDescription('Enable or disable a highlight from being shown by the bot.'),
  execute: async (interaction) => {
    // Check if the user has permission to run the command
    if (!interaction.member.permissions.has('MANAGE_MESSAGES')) {
      await interaction.reply('You do not have permission to manage highlights.');
      return;
    }

    const options = Object.keys(highlightOptions).map((key) => {
      return new StringSelectMenuOptionBuilder()
        .setLabel(key)
        .setDescription(`Current value: ${(highlightOptions[key]).enabled == true ? 'Enabled' : 'Disabled'}`)
        .setValue(key);
    });

    const selectMenu = new StringSelectMenuBuilder()
      .setCustomId('highlight-enable-disable')
      .setPlaceholder('Select a highlight option to enable or disable')
      .addOptions(options);

    const row = new ActionRowBuilder().addComponents(selectMenu);

    await interaction.reply({
      content: 'Select a highlight option to enable/disable:',
      components: [row],
      ephemeral: true,
    });
  },
}

export default togglehighlight;