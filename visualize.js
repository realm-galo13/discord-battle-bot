import { ChartJSNodeCanvas } from 'chartjs-node-canvas';
import fs from 'fs';

async function renderWinStreakChart(data) {
  // Filter out data points with a value of 0
  const filteredWinStreakData = data.filter((item) => item[1] !== 0);

  const labels = filteredWinStreakData.map(entry => entry[0]);
  const winStreaks = filteredWinStreakData.map(entry => entry[1]);

  const width = 1200;
  const height = 800;
  const canvasRenderService = new ChartJSNodeCanvas({ width, height })

  const configuration = {
    type: 'bar',
    data: {
      labels: labels,
      datasets: [
        {
          label: 'Win Streaks',
          data: winStreaks,
          backgroundColor: 'rgba(75, 192, 192, 0.2)',
          borderColor: 'rgba(75, 192, 192, 1)',
          borderWidth: 1,
        },
      ],
    },
    options: {
      scales: {
        y: {
          beginAtZero: true,
        },
      },
      indexAxis: 'x',
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Top Win Streaks',
        },
      },
      responsive: false, // Disable responsive mode for a fixed-size chart
      width: 1200, // Set the chart width
      height: 800, // Set the chart height
      },
  };

  const image = await canvasRenderService.renderToBuffer(configuration);
  fs.writeFile('winStreakChart.png', image, (err) => {
    if (err) {
      console.error('Error saving chart image:', err);
    } else {
      console.log('Chart image saved successfully.');
    }
  });
}

function readWinStreakData(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, 'utf-8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(JSON.parse(data));
      }
    });
  });
}

(async () => {
  try {
    const winStreakData = await readWinStreakData('statistics/results/streaks/win-streaks.json');
    await renderWinStreakChart(winStreakData);
  } catch (err) {
    console.error('Error:', err);
  }
})();
