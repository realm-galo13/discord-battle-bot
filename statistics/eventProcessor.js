import { sortMapByValue } from '../utils/helpers.js';
import { LOW_ODDS_THRESHOLD, UPSET_WIN_THRESHOLD } from '../config/config.js';

export function processFightStartedEvents(eventsFightStarted) {
  const fightStartedByKey = new Map();
  const adventurerBattleCountAsAttacker = new Map();
  const adventurerBattleCountAsOpponent = new Map();

  for (const event of eventsFightStarted) {
    const { transactionHash, fightId, attackerAddr, attackerId, opponentAddr, opponentId } = event;
    const fightKey = `${transactionHash}-${fightId}`;
    fightStartedByKey.set(fightKey, { attackerAddr, attackerId, opponentAddr, opponentId });

    const attackerKey = `${attackerAddr}-${attackerId}`;
    const currentAttackerCount = adventurerBattleCountAsAttacker.get(attackerKey) || 0;
    adventurerBattleCountAsAttacker.set(attackerKey, currentAttackerCount + 1);

    const opponentKey = `${opponentAddr}-${opponentId}`;
    const currentOpponentCount = adventurerBattleCountAsOpponent.get(opponentKey) || 0;
    adventurerBattleCountAsOpponent.set(opponentKey, currentOpponentCount + 1);
  }

  return { fightStartedByKey, adventurerBattleCountAsAttacker, adventurerBattleCountAsOpponent };
}

export function processOverallWinnerEvents(eventsOverallWinner, fightStartedByKey) {
  let winTotals = new Map();
  let lossTotals = new Map();
  let winStreaks = new Map();
  let lossStreaks = new Map();
  let winRates = new Map();
  let calculatedWinRates = new Map();
  let totalAnimaPerBattle = new Map();
  let attackerAnimaWon = 0n;
  let opponentAnimaWon = 0n;
  let adventurerAnimaWonAsAttacker = new Map();
  let adventurerAnimaWonAsOpponent = new Map();

  for (const event of eventsOverallWinner) {
    const { transactionHash, fightId, addr, adventureId, anima } = event;
    const fightKey = `${transactionHash}-${fightId}`;
    const fightData = fightStartedByKey.get(fightKey);

    if (fightData) {
      const { attackerAddr, attackerId, opponentAddr, opponentId } = fightData;

      const isAttackerWinner = (addr === attackerAddr && adventureId === attackerId);
      const winnerKey = `${addr}-${adventureId}`;
      const loserKey = isAttackerWinner ? `${opponentAddr}-${opponentId}` : `${attackerAddr}-${attackerId}`;

      updateWinLossTotals(winTotals, lossTotals, winnerKey, loserKey);
      updateWinLossStreaks(winStreaks, lossStreaks, winnerKey, loserKey);
      updateWinRates(winRates, calculatedWinRates, winnerKey, loserKey);

      attackerAnimaWon += BigInt(isAttackerWinner ? anima : 0n);
      opponentAnimaWon += BigInt(isAttackerWinner ? 0n : anima);

      updateAdventurerAnimaWon(adventurerAnimaWonAsAttacker, adventurerAnimaWonAsOpponent, isAttackerWinner, winnerKey, anima);

      updateTotalAnimaPerBattle(totalAnimaPerBattle, fightKey, anima);
    }
  }

  //Sort the maps by the biggest number
  winTotals = sortMapByValue(winTotals);
  lossTotals = sortMapByValue(lossTotals);
  winStreaks = sortMapByValue(winStreaks);
  lossStreaks = sortMapByValue(lossStreaks);
  winRates = sortMapByValue(calculatedWinRates);  

  return {
    winTotals,
    lossTotals,
    winStreaks,
    lossStreaks,
    winRates,
    totalAnimaPerBattle,
    attackerAnimaWon,
    opponentAnimaWon,
    adventurerAnimaWonAsAttacker,
    adventurerAnimaWonAsOpponent,
  };
}

function updateWinLossTotals(winTotals, lossTotals, winnerKey, loserKey) {
  const winnerWinTotal = winTotals.get(winnerKey) || 0;
  winTotals.set(winnerKey, winnerWinTotal + 1);

  const loserLossTotal = lossTotals.get(loserKey) || 0;
  lossTotals.set(loserKey, loserLossTotal + 1);
}

function updateWinLossStreaks(winStreaks, lossStreaks, winnerKey, loserKey) {
  const winnerWinStreak = winStreaks.get(winnerKey) || 0;
  winStreaks.set(winnerKey, winnerWinStreak + 1);
  lossStreaks.set(winnerKey, 0);

  const loserLossStreak = lossStreaks.get(loserKey) || 0;
  lossStreaks.set(loserKey, loserLossStreak + 1);
  winStreaks.set(loserKey, 0);
}

function updateWinRates(winRates, calculatedWinRates, winnerKey, loserKey) {
  const winnerWinRateData = winRates.get(winnerKey) || { wins: 0, total: 0 };
  winRates.set(winnerKey, { wins: winnerWinRateData.wins + 1, total: winnerWinRateData.total + 1 });
  for (const [winnerKey, { wins, total }] of winRates.entries()) {
    calculatedWinRates.set(winnerKey, parseFloat(wins) / total);
  }

  const loserWinRateData = winRates.get(loserKey) || { wins: 0, total: 0 };
  winRates.set(loserKey, { wins: loserWinRateData.wins, total: loserWinRateData.total + 1 });
  for (const [loserKey, { wins, total }] of winRates.entries()) {
    calculatedWinRates.set(loserKey, parseFloat(wins) / total);
  }
}

function updateAdventurerAnimaWon(adventurerAnimaWonAsAttacker, adventurerAnimaWonAsOpponent, isAttackerWinner, winnerKey, anima) {
  if (isAttackerWinner) {
    const adventurerAnimaWonAsAttackerData = adventurerAnimaWonAsAttacker.get(winnerKey) || 0n;
    adventurerAnimaWonAsAttacker.set(winnerKey, adventurerAnimaWonAsAttackerData + BigInt(anima));
  } else {
    const adventurerAnimaWonAsOpponentData = adventurerAnimaWonAsOpponent.get(winnerKey) || 0n;
    adventurerAnimaWonAsOpponent.set(winnerKey, adventurerAnimaWonAsOpponentData + BigInt(anima));
  }
}

function updateTotalAnimaPerBattle(totalAnimaPerBattle, fightKey, anima) {
  const totalAnimaPerBattleData = totalAnimaPerBattle.get(fightKey) || 0n;
  totalAnimaPerBattle.set(fightKey, totalAnimaPerBattleData + BigInt(anima));
}

export function processContenderRollEvents(eventsContenderRoll, fightStartedByKey, totalAnimaPerBattle, attackerAnimaWon, opponentAnimaWon, adventurerAnimaWonAsAttacker, adventurerAnimaWonAsOpponent) {
  // Initialize variables to store statistics
  let upsetWinsTraitBattles = 0;
  let upsetWinsPerBattle = new Map();
  let attackerUpsetWinsPerBattle = new Map();
  let opponentUpsetWinsPerBattle = new Map();
  let attackerTotalUpsetWins = new Map();
  let opponentTotalUpsetWins = new Map();
  let traitBattlesWonWithLowOdds = 0;
  let lowOddsWinsPerBattle = new Map();
  let traitBattlesWonPerBattle = new Map();  
  
  for (const event of eventsContenderRoll) {
    const { transactionHash, fightId, winnerAddr, winnerId, probability, animaReward, animaBonus, attackerReward } = event;
    const fightKey = `${transactionHash}-${fightId}`;
    const fightData = fightStartedByKey.get(fightKey);

    if (fightData) {
      const { attackerAddr, attackerId, opponentAddr, opponentId } = fightData;

      // Check if the winner is the attacker or the opponent
      const isAttackerWinner = (winnerAddr === attackerAddr && winnerId === attackerId);
      const winnerKey = `${winnerAddr}-${winnerId}`;
      const loserKey = isAttackerWinner ? `${opponentAddr}-${opponentId}` : `${attackerAddr}-${attackerId}`;

      //If the winner is the attacker, add the animaReward, attackerReward and animaBonus to attackerAnimaWon
      if (isAttackerWinner) {
        let animaWonTrait = BigInt(animaReward) + BigInt(animaBonus) + BigInt(attackerReward);
        attackerAnimaWon += animaWonTrait;

        const currentWinnerAnima = adventurerAnimaWonAsAttacker.get(winnerKey) || 0;
        adventurerAnimaWonAsAttacker.set(winnerKey, BigInt(currentWinnerAnima) + animaWonTrait);
        //If the winner is the opponent, add the animaReward and animaBonus to opponentAnimaWon, and add the attackerReward to attackerAnimaWon
      } else {
        let animaWonTrait = BigInt(animaReward) + BigInt(animaBonus);
        let animaWonTraitAttackerReward = BigInt(attackerReward);
        opponentAnimaWon += animaWonTrait;
        attackerAnimaWon += animaWonTraitAttackerReward;

        const currentWinnerAnima = adventurerAnimaWonAsOpponent.get(winnerKey) || 0;
        adventurerAnimaWonAsOpponent.set(winnerKey, BigInt(currentWinnerAnima) + animaWonTrait);
        const currentAttackerAnima = adventurerAnimaWonAsAttacker.get(winnerKey) || 0;
        adventurerAnimaWonAsAttacker.set(winnerKey, BigInt(currentAttackerAnima) + BigInt(animaWonTraitAttackerReward));
      }

      // Count battles with low winning odds
      if (probability <= LOW_ODDS_THRESHOLD) {
        traitBattlesWonWithLowOdds++;
        const currentCount = lowOddsWinsPerBattle.get(fightKey) || 0;
        lowOddsWinsPerBattle.set(fightKey, currentCount + 1);
      }
      // Count battles with upset wins
      if (probability < UPSET_WIN_THRESHOLD) {
        upsetWinsTraitBattles++;
        const currentCount = upsetWinsPerBattle.get(fightKey) || 0;
        upsetWinsPerBattle.set(fightKey, currentCount + 1);
        if (isAttackerWinner) {
          const currentAttackerUpsetWinCount = attackerUpsetWinsPerBattle.get(fightKey) || 0;
          attackerUpsetWinsPerBattle.set(fightKey, currentAttackerUpsetWinCount + 1);
          const attackerTotalUpsetWinCount = attackerTotalUpsetWins.get(winnerKey) || 0;
          attackerTotalUpsetWins.set(winnerKey, attackerTotalUpsetWinCount + 1);
        }
        else {
          const currentOpponentUpsetWinCount = opponentUpsetWinsPerBattle.get(fightKey) || 0;
          opponentUpsetWinsPerBattle.set(fightKey, currentOpponentUpsetWinCount + 1);
          const opponentTotalUpsetWinCount = opponentTotalUpsetWins.get(winnerKey) || 0;
          opponentTotalUpsetWins.set(winnerKey, opponentTotalUpsetWinCount + 1);
        }
      }

      // Update total anima per battle
      const totalAnimaPerBattleData = totalAnimaPerBattle.get(fightKey) || 0n;
      totalAnimaPerBattle.set(fightKey, totalAnimaPerBattleData + BigInt(animaReward) + BigInt(animaBonus) + BigInt(attackerReward));

      // Update amount of trait battles won by the attacker and the opponent, per battle
      const currentWins = traitBattlesWonPerBattle.get(fightKey) || { attackerWins: 0, opponentWins: 0 };
      if (isAttackerWinner) {
        currentWins.attackerWins++;
      }
      else {
        currentWins.opponentWins++;
      }
      traitBattlesWonPerBattle.set(fightKey, currentWins);

    }
  }
  return {
    upsetWinsTraitBattles,
    upsetWinsPerBattle,
    attackerUpsetWinsPerBattle,
    opponentUpsetWinsPerBattle,
    attackerTotalUpsetWins,
    opponentTotalUpsetWins,
    traitBattlesWonWithLowOdds,
    lowOddsWinsPerBattle,
    traitBattlesWonPerBattle,
    totalAnimaPerBattle,
    attackerAnimaWon,
    opponentAnimaWon,
    adventurerAnimaWonAsAttacker,
    adventurerAnimaWonAsOpponent,
  };
}