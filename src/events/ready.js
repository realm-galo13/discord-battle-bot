import { initializeStatisticsData } from "../updateStatisticsMemory.js";

export default function (client) {
  client.once('ready', () => {
    console.log(`Logged in as ${client.user.tag}`);

    // Initialize statistics data
    initializeStatisticsData();
  });
}