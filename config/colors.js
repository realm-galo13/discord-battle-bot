export const COLORS = {
  DARK_GREEN: '#60d394',
  LIGHT_GREEN: '#aaf683',
  RED: '#ee6055',
  YELLOW: '#ffd97d',
  ORANGE: '#ff7f00',
  PINK: '#ff9b85',
  BLUE: '#5bc0de',
  BLACK: '#000000',
  DARK_GRAY: '#2b2d31' //rgb(43,45,49)
};