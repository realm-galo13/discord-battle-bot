import fs from 'fs';
import path from 'path';
import { getRivalryData } from '../updateStatisticsMemory.js';

const rivalryFilePath = path.join('statistics', 'results', 'rivalry', 'rivalry.json');

export function checkRivalryHighlight(battle, options) {
  const attacker = `${battle.fightStartedData.attackerAddr}-${battle.fightStartedData.attackerId}`;
  const opponent = `${battle.fightStartedData.opponentAddr}-${battle.fightStartedData.opponentId}`;
  const key = [attacker, opponent].sort().join('/');

  //const rivalryData = JSON.parse(fs.readFileSync(rivalryFilePath, 'utf-8'));
  const rivalryData = getRivalryData();
  const rivalryEntry = rivalryData.find(([entryKey]) => entryKey === key);

  if (rivalryEntry && rivalryEntry[1] >= options.minEncounters) {
    return rivalryEntry;
  }

  return false;
}