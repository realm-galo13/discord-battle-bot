import fs from 'fs';
import path from 'path';
import { getWinStreaksData } from '../updateStatisticsMemory.js';

const winStreakFilePath = path.join('statistics', 'results', 'streaks', 'win-streaks.json');

export function checkWinStreakHighlight(battle, options) {
  const winnerKey = `${battle.overallWinnerData.addr}-${battle.overallWinnerData.adventureId}`;
  //const winStreaks = JSON.parse(fs.readFileSync(winStreakFilePath, 'utf-8'));
  const winStreaks = getWinStreaksData();

  for (const [adventurerKey, winStreak] of winStreaks) {
    if (adventurerKey === winnerKey && winStreak >= options.minWinStreak) {
      return (winStreak + 1);
    }
  }

  return false;
}