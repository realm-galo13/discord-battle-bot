import { ethers } from 'ethers';
import { fetchDeploymentTransactionArbiscan, getEarliestBlockFromLogs, getLatestBlockFromLogs } from '../../utils/helpers.js';
import { fetchLogsInRange } from '../../statistics/eventFetcher.js';
import { CHUNK_SIZE, FILES_CHUNK_SIZE } from '../../config/config.js';

export class MyContract {
  constructor(address, abi, provider, endBlock = null) {
    this.address = address;
    this.abi = abi;
    this.provider = provider;
    this.startBlock = null;
    this.endBlock = endBlock;
    this.contractInstance = new ethers.Contract(address, abi, provider);
  }

  static async create(address, abi, provider, endBlock = null) {
    const contract = new MyContract(address, abi, provider, endBlock);
    await contract.setStartBlock();
    return contract;
  }

  async fetchEvents() {
    const startBlock = this.startBlock;
    const endBlock = this.endBlock || await this.provider.getBlockNumber();
  
    const latestBlockOnFile = await getLatestBlockFromLogs(this.address);
    const earliestBlockOnFile = await getEarliestBlockFromLogs(this.address);
  
    if (startBlock >= latestBlockOnFile && endBlock <= earliestBlockOnFile) {
      console.log("Logs for this contract are already written to disk.");
      return;
    }
  
    // Fetch all logs if none are written to disk
    if (latestBlockOnFile === -1 && earliestBlockOnFile === -1) {
      console.log('Fetching all logs...');
      await fetchLogsInRange(this, startBlock, endBlock, CHUNK_SIZE, FILES_CHUNK_SIZE);
    } else {
      // Fetch newer logs
      if (endBlock > latestBlockOnFile) {
        console.log('Fetching newer logs...');
        await fetchLogsInRange(this, latestBlockOnFile + 1, endBlock, CHUNK_SIZE, FILES_CHUNK_SIZE);
      }

      // Fetch older logs
      if (startBlock < earliestBlockOnFile) {
        console.log('Fetching older logs...');
        await fetchLogsInRange(this, startBlock, earliestBlockOnFile - 1, CHUNK_SIZE, FILES_CHUNK_SIZE);
      }
    }    
    console.log('All missing events fetched and saved to disk.');
  }  

  async setStartBlock() {
    const deploymentTx = await fetchDeploymentTransactionArbiscan(this.address);  
    this.startBlock = parseInt(deploymentTx.blockNumber, 10);
  
    // Log to check if the contract was built correctly
    console.log(`Contract created with address: ${this.address}, startBlock: ${this.startBlock}, endBlock: ${this.endBlock}`);
  }
}