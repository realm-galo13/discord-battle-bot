import { SlashCommandBuilder, ActionRowBuilder, StringSelectMenuBuilder, StringSelectMenuOptionBuilder } from '@discordjs/builders';
import highlightOptions from '../../config/highlightOptions.js';

const updatehighlight = {
  data: new SlashCommandBuilder()
    .setName('updatehighlight')
    .setDescription('Update a highlight option.'),
  execute: async (interaction) => {
    // Check if the user has permission to run the command
    if (!interaction.member.permissions.has('MANAGE_MESSAGES')) {
      await interaction.reply('You do not have permission to manage highlights.');
      return;
    }

    const options = [];
    Object.keys(highlightOptions).forEach((optionName) => {
      Object.keys(highlightOptions[optionName]).forEach((attributeName) => {
        if (attributeName !== 'enabled') {
          const label = `${optionName} -> ${attributeName}`;
          const option = new StringSelectMenuOptionBuilder()
            .setLabel(label)
            .setDescription(`Current value: ${highlightOptions[optionName][attributeName]}`)
            .setValue(`${optionName}:${attributeName}`);
          options.push(option);
        }
      });
    });

    const selectMenu = new StringSelectMenuBuilder()
      .setCustomId('highlight-update-select')
      .setPlaceholder('Select a highlight option to update')
      .addOptions(options);

    const row = new ActionRowBuilder().addComponents(selectMenu);

    await interaction.reply({
      content: 'Select a highlight option to update:',
      components: [row],
      ephemeral: true,
    });
  },
}

export default updatehighlight;