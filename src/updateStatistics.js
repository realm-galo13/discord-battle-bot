//TODO TESTING
import fs from 'fs';
import path from 'path';
import { TRAIT_NAMES } from '../config/config.js';

const winStreaksPath = path.join('statistics', 'results', 'streaks', 'win-streaks.json');
const lossStreaksPath = path.join('statistics', 'results', 'streaks', 'loss-streaks.json');
const rivalryPath = path.join('statistics', 'results', 'rivalry', 'rivalry.json');

function updateWinStreaks(battle) {
  const winStreaksData = JSON.parse(fs.readFileSync(winStreaksPath, 'utf-8'));

  const winner = battle.getWinner();
  const winnerKey = `${winner.address}-${winner.id}`;

  let winnerEntry = winStreaksData.find(([adventurerKey]) => adventurerKey === winnerKey);

  if (winnerEntry) {
    winnerEntry[1]++;
  } else {
    winnerEntry = [winnerKey, 1];
    winStreaksData.push(winnerEntry);
  }

  winStreaksData.sort((a, b) => b[1] - a[1]);
  fs.writeFileSync(winStreaksPath, JSON.stringify(winStreaksData, null, 2));
}

function updateLossStreaks(battle) {
  const lossStreaksData = JSON.parse(fs.readFileSync(lossStreaksPath, 'utf-8'));

  const loser = battle.getLoser();
  const loserKey = `${loser.address}-${loser.id}`;

  let loserEntry = lossStreaksData.find(([adventurerKey]) => adventurerKey === loserKey);

  if (loserEntry) {
    loserEntry[1]++;
  } else {
    loserEntry = [loserKey, 1];
    lossStreaksData.push(loserEntry);
  }

  lossStreaksData.sort((a, b) => b[1] - a[1]);
  fs.writeFileSync(lossStreaksPath, JSON.stringify(lossStreaksData, null, 2));
}

function updateRivalry(battle) {
  const rivalryData = JSON.parse(fs.readFileSync(rivalryPath, 'utf-8'));

  const winner = battle.getWinner();
  const loser = battle.getLoser();
  const winnerKey = `${winner.address}-${winner.id}`;
  const loserKey = `${loser.address}-${loser.id}`;

  const sortedKeys = [winnerKey, loserKey].sort();
  const key = sortedKeys.join('/');

  let rivalryEntry = rivalryData.find(([entryKey]) => entryKey === key);

  if (rivalryEntry) {
    rivalryEntry[1]++; // Increment the number of fights between the two adventurers

    // Increment the winner's win count (position 2 for winnerKey or 3 for loserKey)
    rivalryEntry[sortedKeys.indexOf(winnerKey) + 2]++;
  } else {
    // Create a new entry with 1 fight and 1 win for the winner
    rivalryEntry = [key, 1, sortedKeys.indexOf(winnerKey) === 0 ? 1 : 0, sortedKeys.indexOf(winnerKey) === 1 ? 1 : 0];
    rivalryData.push(rivalryEntry);
  }

  fs.writeFileSync(rivalryPath, JSON.stringify(rivalryData, null, 2));
}

async function updateTraitMastery(battle) {
  if (!battle.isComplete()) {
    return;
  }

  const traitMasteryFolderPath = path.join('statistics', 'results', 'traitMastery');
  const traitFiles = Object.fromEntries(
    Object.entries(TRAIT_NAMES).map(([key, value]) => [value, `${value}Mastery.json`])
  );

  // Load trait files
  const traitStats = {};
  for (const [trait, file] of Object.entries(traitFiles)) {
    const filePath = path.join(traitMasteryFolderPath, file);
    if (fs.existsSync(filePath)) {
      traitStats[trait] = JSON.parse(fs.readFileSync(filePath));
    } else {
      traitStats[trait] = [];
    }
  }

  // Update trait stats using battle data
  battle.contenderRollsData.forEach((contenderRoll) => {
    const winner = `${contenderRoll.winnerAddr}-${contenderRoll.winnerId}`;
    const loser = battle.getLoser().address === contenderRoll.winnerAddr
      ? `${battle.getWinner().address}-${battle.getWinner().id}`
      : `${battle.getLoser().address}-${battle.getLoser().id}`;

    const trait = TRAIT_NAMES[contenderRoll.traitId];

    const winnerEntry = traitStats[trait].find((entry) => entry.adventurer === winner);
    const loserEntry = traitStats[trait].find((entry) => entry.adventurer === loser);

    if (!winnerEntry) {
      traitStats[trait].push({
        adventurer: winner,
        battles: 1,
        [trait]: { wins: 1, losses: 0 },
      });
    } else {
      winnerEntry.battles++;
      winnerEntry[trait].wins++;
    }

    if (!loserEntry) {
      traitStats[trait].push({
        adventurer: loser,
        battles: 1,
        [trait]: { wins: 0, losses: 1 },
      });
    } else {
      loserEntry.battles++;
      loserEntry[trait].losses++;
    }
  });

  // Save updated trait stats
  for (const [trait, file] of Object.entries(traitFiles)) {
    // Sort the entries by win ratio in the given trait
    traitStats[trait].sort((a, b) => {
      const aWinRatio = a[trait].wins / (a[trait].wins + a[trait].losses);
      const bWinRatio = b[trait].wins / (b[trait].wins + b[trait].losses);
      return bWinRatio - aWinRatio;
    });

    const filePath = path.join(traitMasteryFolderPath, file);
    await mkdirp(path.dirname(filePath));

    await new Promise((resolve, reject) => {
      fs.writeFile(filePath, JSON.stringify(traitStats[trait], null, 2), (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
}

async function updateAverageAnimaWon(battle) {
  if (!battle.isComplete()) {
    return;
  }

  const filePaths = {
    attacker: path.join('statistics', 'results', 'anima', 'average-adventurer-anima-won-as-attacker.json'),
    opponent: path.join('statistics', 'results', 'anima', 'average-adventurer-anima-won-as-opponent.json'),
  };

  const animaStats = {
    attacker: [],
    opponent: [],
  };

  for (const key of ['attacker', 'opponent']) {
    if (fs.existsSync(filePaths[key])) {
      animaStats[key] = JSON.parse(fs.readFileSync(filePaths[key]));
    }
  }

  const participants = {
    attacker: `${battle.fightStartedData.attackerAddr}-${battle.fightStartedData.attackerId}`,
    opponent: `${battle.fightStartedData.opponentAddr}-${battle.fightStartedData.opponentId}`,
  };

  const animaValues = {
    attacker: battle.getAttackerAnima(),
    opponent: battle.getOpponentAnima(),
  };

  for (const key of ['attacker', 'opponent']) {
    const entryIndex = animaStats[key].findIndex(([adventurer]) => adventurer === participants[key]);

    if (entryIndex === -1) {
      animaStats[key].push([
        participants[key],
        {
          battleCount: 1,
          averageAnima: animaValues[key],
        },
      ]);
    } else {
      const entry = animaStats[key][entryIndex];
      const updatedBattleCount = entry[1].battleCount + 1;
      const updatedAverageAnima = (entry[1].averageAnima * entry[1].battleCount + animaValues[key]) / updatedBattleCount;

      animaStats[key][entryIndex] = [
        participants[key],
        {
          battleCount: updatedBattleCount,
          averageAnima: updatedAverageAnima,
        },
      ];
    }

    await mkdirp(path.dirname(filePaths[key]));

    await new Promise((resolve, reject) => {
      fs.writeFile(filePaths[key], JSON.stringify(animaStats[key], null, 2), (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
}

export function updateStatistics(battle) {
  updateWinStreaks(battle);
  updateLossStreaks(battle);
  updateRivalry(battle);
  updateTraitMastery(battle);
  updateAverageAnimaWon(battle);
}