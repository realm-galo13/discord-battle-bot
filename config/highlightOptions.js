// Description: This file contains the configuration for the highlight options.
// The options are used to determine whether a highlight should be displayed
// in the UI. The constants defined here are used in the src/highlights check
// functions, entering as the 'options' parameter.

const winStreak = {
  minWinStreak: 8,
  enabled: true,
};

const lossStreak = {
  minLossStreak: 5,
  enabled: true,
};

const topPercentAnimaAttackers = {
  topPercent: 5,
  enabled: true,
};

const topPercentAnimaOpponents = {
  topPercent: 3,
  enabled: true,
};

const lowOddWins = {
  minLowOddWins: 2,
  lowOddThreshold: 10,
  enabled: true,
};

const flawlessVictory = {
  enabled: true,
};

const upsetWins = {
  minUpsetWins: 3,
  enabled: true
};

const rivalry = {
  minEncounters: 4,
  enabled: true,
};

const traitMaster = {
  topPercent: 3,
  minBattleCount: 25,
  enabled: true,
};

export default {
  winStreak,
  lossStreak,
  topPercentAnimaAttackers,
  topPercentAnimaOpponents,
  lowOddWins,
  flawlessVictory,
  upsetWins,
  rivalry,
  traitMaster,
};