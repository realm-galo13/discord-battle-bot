import { ethers } from 'ethers';
import dotenv from 'dotenv';
import { MyContract } from './src/classes/MyContract.js';
import { CONTRACTS, LOW_ODDS_THRESHOLD, FETCH_EVENTS } from './config/config.js';
import { writeToFile, appendToFile, sortMapByValue, convertMapToFormattedStrings, calculateAverageAdventurerAnimaWon, calculatePercentageOfBattlesWithAtLeastNUpsetWins, divideByDecimalFactor } from './utils/helpers.js';
import { readEventsFromDisk } from './statistics/eventFetcher.js';
import { processFightStartedEvents, processOverallWinnerEvents, processContenderRollEvents } from './statistics/eventProcessor.js';
import { generateRivalryStats, saveRivalryToDisk } from './statistics/rivalry.js';
import { generateTraitMasteryStats, saveTraitMasteryToDisk } from './statistics/traitMastery.js';

dotenv.config();

// Load the ABIs and the contract addresses
const provider = new ethers.JsonRpcProvider(process.env.PROVIDER_URL);

async function main() {
  const battleContractGroup = CONTRACTS.find((group) => group.type === 'battle');
  const contractPromises = battleContractGroup.contracts.map((details) => MyContract.create(details.address, details.abi, provider, details.endBlock));
  const contracts = await Promise.all(contractPromises);

  if (FETCH_EVENTS) {
    for (const contract of contracts) {
      await contract.fetchEvents();
    }
  }

  var {
    blockRange,
    eventsFightStarted,
    eventsContenderRoll,
    eventsOverallWinner,
  } = await readEventsFromDisk();

  const traitMasteryMap = await generateTraitMasteryStats(eventsFightStarted, eventsContenderRoll);
  await saveTraitMasteryToDisk(traitMasteryMap);


  const rivalryMap = await generateRivalryStats(eventsFightStarted, eventsOverallWinner);
  await saveRivalryToDisk(rivalryMap);
  
  const {
    fightStartedByKey,
    adventurerBattleCountAsAttacker,
    adventurerBattleCountAsOpponent,
  } = processFightStartedEvents(eventsFightStarted);

  let {
    winTotals,
    lossTotals,
    winStreaks,
    lossStreaks,
    winRates,
    totalAnimaPerBattle: totalAnimaPerBattleOverallWinnerEvents,
    attackerAnimaWon: attackerAnimaWonOverallWinnerEvents,
    opponentAnimaWon: opponentAnimaWonOverallWinnerEvents,
    adventurerAnimaWonAsAttacker: adventurerAnimaWonAsAttackerOverallWinnerEvents,
    adventurerAnimaWonAsOpponent: adventurerAnimaWonAsOpponentOverallWinnerEvents,
  } = processOverallWinnerEvents(eventsOverallWinner, fightStartedByKey);

  let {
    upsetWinsTraitBattles,
    upsetWinsPerBattle,
    attackerUpsetWinsPerBattle,
    opponentUpsetWinsPerBattle,
    attackerTotalUpsetWins,
    opponentTotalUpsetWins,
    traitBattlesWonWithLowOdds,
    lowOddsWinsPerBattle,
    traitBattlesWonPerBattle,
    totalAnimaPerBattle,
    attackerAnimaWon,
    opponentAnimaWon,
    adventurerAnimaWonAsAttacker,
    adventurerAnimaWonAsOpponent,
  } = processContenderRollEvents(eventsContenderRoll, fightStartedByKey, totalAnimaPerBattleOverallWinnerEvents, attackerAnimaWonOverallWinnerEvents, opponentAnimaWonOverallWinnerEvents, adventurerAnimaWonAsAttackerOverallWinnerEvents, adventurerAnimaWonAsOpponentOverallWinnerEvents);

  // Calculate average anima rewards
  const averageAttackerAnimaWon = attackerAnimaWon / BigInt(eventsOverallWinner.length);
  const averageOpponentAnimaWon = opponentAnimaWon / BigInt(eventsOverallWinner.length);

  // Calculate number of battles where attacker or opponent won all 6 traits
  const attackerWinsAllTraits = Array.from(traitBattlesWonPerBattle.values()).filter(({ attackerWins }) => attackerWins === 6).length;
  const opponentWinsAllTraits = Array.from(traitBattlesWonPerBattle.values()).filter(({ opponentWins }) => opponentWins === 6).length;

  // Calculate the percentage of battles where attacker or opponent won all 6 traits
  const attackerWinsAllTraitsPercentage = (attackerWinsAllTraits / eventsOverallWinner.length) * 100;
  const opponentWinsAllTraitsPercentage = (opponentWinsAllTraits / eventsOverallWinner.length) * 100;

  //Sort the maps by the biggest number and convert to formatted strings
  totalAnimaPerBattle = sortMapByValue(totalAnimaPerBattle);
  adventurerAnimaWonAsAttacker = sortMapByValue(adventurerAnimaWonAsAttacker);
  adventurerAnimaWonAsOpponent = sortMapByValue(adventurerAnimaWonAsOpponent);
  totalAnimaPerBattle = convertMapToFormattedStrings(totalAnimaPerBattle);
  adventurerAnimaWonAsAttacker = convertMapToFormattedStrings(adventurerAnimaWonAsAttacker);
  adventurerAnimaWonAsOpponent = convertMapToFormattedStrings(adventurerAnimaWonAsOpponent);

  //Calculate the average anima won by each adventurer as attacker and opponent
  let averageAdventurerAnimaWonAsAttacker = calculateAverageAdventurerAnimaWon(adventurerAnimaWonAsAttacker, adventurerBattleCountAsAttacker);
  let averageAdventurerAnimaWonAsOpponent = calculateAverageAdventurerAnimaWon(adventurerAnimaWonAsOpponent, adventurerBattleCountAsOpponent);
  averageAdventurerAnimaWonAsAttacker = sortMapByValue(averageAdventurerAnimaWonAsAttacker);
  averageAdventurerAnimaWonAsOpponent = sortMapByValue(averageAdventurerAnimaWonAsOpponent);

  //Calculate the percentage of battles won with low odds
  const percentageTraitBattlesWonWithLowOdds = traitBattlesWonWithLowOdds / eventsContenderRoll.length * 100;
  //Calculate the percentage of battles having at least one very unlikely win
  const percentageBattlesWithLowOddsWins = lowOddsWinsPerBattle.size / eventsOverallWinner.length * 100;
  //Calculate the percentage of battles won with upset odds
  const percentageTraitBattlesWonWithUpsetOdds = upsetWinsTraitBattles / eventsContenderRoll.length * 100;
  // Calculate the percentage of battles having at least 1 to 6 upset wins
  const percentages = {};
  for (let n = 1; n <= 6; n++) {
    const percentageOfBattlesWithAtLeastNUpsetWins = calculatePercentageOfBattlesWithAtLeastNUpsetWins(upsetWinsPerBattle, eventsOverallWinner.length, n);
    percentages[n] = percentageOfBattlesWithAtLeastNUpsetWins.toFixed(2);
  }

  //Calculate the percentage of battles having at least 1 to 6 upset wins for the attacker
  const percentagesAttacker = {};
  for (let n = 1; n <= 6; n++) {
    const percentageOfBattlesWithAtLeastNUpsetWins = calculatePercentageOfBattlesWithAtLeastNUpsetWins(attackerUpsetWinsPerBattle, eventsOverallWinner.length, n);
    percentagesAttacker[n] = percentageOfBattlesWithAtLeastNUpsetWins.toFixed(2);
  }
  //Calculate the percentage of battles having at least 1 to 6 upset wins for the opponent
  const percentagesOpponent = {};
  for (let n = 1; n <= 6; n++) {
    const percentageOfBattlesWithAtLeastNUpsetWins = calculatePercentageOfBattlesWithAtLeastNUpsetWins(opponentUpsetWinsPerBattle, eventsOverallWinner.length, n);
    percentagesOpponent[n] = percentageOfBattlesWithAtLeastNUpsetWins.toFixed(2);
  }

  //Sort the maps by the biggest number
  lowOddsWinsPerBattle = sortMapByValue(lowOddsWinsPerBattle);
  upsetWinsPerBattle = sortMapByValue(upsetWinsPerBattle);
  attackerUpsetWinsPerBattle = sortMapByValue(attackerUpsetWinsPerBattle);
  opponentUpsetWinsPerBattle = sortMapByValue(opponentUpsetWinsPerBattle);
  attackerTotalUpsetWins = sortMapByValue(attackerTotalUpsetWins);
  opponentTotalUpsetWins = sortMapByValue(opponentTotalUpsetWins);  

  // Write the sorted maps to files
  await Promise.all([
    writeToFile('statistics/results/totals/win-totals.json', Array.from(winTotals)),
    writeToFile('statistics/results/totals/loss-totals.json', Array.from(lossTotals)),
    writeToFile('statistics/results/streaks/win-streaks.json', Array.from(winStreaks)),
    writeToFile('statistics/results/streaks/loss-streaks.json', Array.from(lossStreaks)),
    writeToFile('statistics/results/totals/win-rates.json', Array.from(winRates)),
    writeToFile('statistics/results/anima/total-anima-per-battle.json', Array.from(totalAnimaPerBattle)),
    writeToFile('statistics/results/anima/adventurer-anima-won-as-attacker.json', Array.from(adventurerAnimaWonAsAttacker)),
    writeToFile('statistics/results/anima/adventurer-anima-won-as-opponent.json', Array.from(adventurerAnimaWonAsOpponent)),
    writeToFile('statistics/results/anima/average-adventurer-anima-won-as-attacker.json', Array.from(averageAdventurerAnimaWonAsAttacker)),
    writeToFile('statistics/results/anima/average-adventurer-anima-won-as-opponent.json', Array.from(averageAdventurerAnimaWonAsOpponent)),
    writeToFile('statistics/results/lowOdds/low-odds-wins-per-battle.json', Array.from(lowOddsWinsPerBattle)),
    writeToFile('statistics/results/upsetWins/upset-wins-per-battle.json', Array.from(upsetWinsPerBattle)),
    writeToFile('statistics/results/upsetWins/attacker-upset-wins-per-battle.json', Array.from(attackerUpsetWinsPerBattle)),
    writeToFile('statistics/results/upsetWins/opponent-upset-wins-per-battle.json', Array.from(opponentUpsetWinsPerBattle)),
    writeToFile('statistics/results/upsetWins/attacker-total-upset-wins.json', Array.from(attackerTotalUpsetWins)),
    writeToFile('statistics/results/upsetWins/opponent-total-upset-wins.json', Array.from(opponentTotalUpsetWins)),
    appendToFile('statistics/results/totals/general.json', JSON.stringify({
      "Number of Battles": eventsOverallWinner.length,
      "Low Odds Threshold": LOW_ODDS_THRESHOLD,
      "Total Anima Won by Attackers": divideByDecimalFactor(attackerAnimaWon, 18),
      "Total Anima Won by Opponents": divideByDecimalFactor(opponentAnimaWon, 18),
      "Total Anima Won": divideByDecimalFactor((attackerAnimaWon + opponentAnimaWon), 18),
      "Average Anima Won by Attacker": divideByDecimalFactor(averageAttackerAnimaWon, 18),
      "Average Anima Won by Opponent": divideByDecimalFactor(averageOpponentAnimaWon, 18),
      "Average Anima Won": divideByDecimalFactor((averageAttackerAnimaWon + averageOpponentAnimaWon), 18),
      "Percentage of trait Battles with Upset Wins": percentageTraitBattlesWonWithUpsetOdds,
      "Percentage of Battles with at least 1 to 6 Upset Wins": percentages,
      "Percentage of Battles with at least 1 to 6 Upset Wins for the Attacker": percentagesAttacker,
      "Percentage of Battles with at least 1 to 6 Upset Wins for the Opponent": percentagesOpponent,
      "Percentage of trait Battles Won with Low Odds": percentageTraitBattlesWonWithLowOdds,
      "Percentage of Battles with Low Odds Wins": percentageBattlesWithLowOddsWins,
      "Percentage of Battles where attacker won all 6 rolls": attackerWinsAllTraitsPercentage,
      "Percentage of Battles where opponent won all 6 rolls": opponentWinsAllTraitsPercentage,
    }, null, 2) + '\n'),
  ]);
  console.log('Statistics generated!');
}

main().catch(console.error);