import sharp from 'sharp';
import axios from 'axios';
import fs from 'fs';
import { promisify } from 'util';
import path from 'path';
import { mkdirp } from 'mkdirp';
import { getFilenameFromUrl, withRetries } from './helpers.js';
import { getNextIPFSGateway } from './ipfsGateway.js';
import { IPFS_GATEWAYS } from '../config/config.js';

const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);

// Download an image from a URL and save it to disk. If the image is already on
// disk, just read it from there.
async function downloadImage(url) {
  const filename = getFilenameFromUrl(url);
  const folderPath = path.join('assets', 'variants');
  const filepath = path.join(folderPath, filename);

  // Check if the image is already on disk
  if (fs.existsSync(filepath)) {
    return await readFileAsync(filepath);
  }

  try {
    // Create the directory if it doesn't exist
    await mkdirp(folderPath);

    const response = await axios.get(url, { responseType: 'arraybuffer' });
    const imageData = Buffer.from(response.data, 'binary');

    // Save the image to disk before returning it
    await writeFileAsync(filepath, imageData);

    return imageData;
  } catch (error) {
    console.error(`Error downloading image: ${url}`, error);
    throw error;
  }
}

// Download an image from IPFS and save it to disk. If the image is already on
// disk, just read it from there.
async function downloadImageIPFS(ipfsHash) {
  const filename = ipfsHash.split('/').pop();
  const folderPath = path.join('assets', 'variants');
  const filepath = path.join(folderPath, filename);

  // Check if the image is already on disk
  if (fs.existsSync(filepath)) {
    return await readFileAsync(filepath);
  }

  // Create a function to be used with withRetries
  const fetchImage = async () => {
    const ipfsImageUrl = `${getNextIPFSGateway()}/ipfs/${ipfsHash}`;

    try {
      // Create the directory if it doesn't exist
      await mkdirp(folderPath);

      const response = await axios.get(ipfsImageUrl, { responseType: 'arraybuffer' });
      const imageData = Buffer.from(response.data, 'binary');

      // Save the image to disk before returning it
      await writeFileAsync(filepath, imageData);

      return imageData;
    } catch (error) {
      console.error(`Error downloading image: ${ipfsImageUrl}`, error);
      throw error;
    }
  };

  // Use withRetries to download the image with retries and changing IPFS gateway
  return await withRetries(fetchImage, IPFS_GATEWAYS.length - 1);
}

// Combine two images side-by-side with a VS image in the middle
export async function combineImages(winnerImageIPFSHash, loserImageIPFSHash, vsImageUrl) {
  try {
    const winnerImageData = await downloadImageIPFS(winnerImageIPFSHash);
    const loserImageData = await downloadImageIPFS(loserImageIPFSHash);
    const vsImageData = await downloadImage(vsImageUrl);

    const winnerImage = sharp(winnerImageData);
    const loserImage = sharp(loserImageData);
    const vsImage = sharp(vsImageData);

    const winnerMetadata = await winnerImage.metadata();
    const loserMetadata = await loserImage.metadata();
    const vsMetadata = await vsImage.metadata();

    const borderWidth = Math.round(winnerMetadata.width * 0.03);
    const spacerWidth = Math.round(winnerMetadata.width * 0.4);

    const canvasWidth =
      winnerMetadata.width + loserMetadata.width + spacerWidth + borderWidth * 2;
    const canvasHeight = Math.max(
      winnerMetadata.height,
      loserMetadata.height
    ) + borderWidth * 2;

    const combinedImages = await sharp({
      create: {
        width: canvasWidth,
        height: canvasHeight,
        channels: 4,
        background: { r: 43, g: 45, b: 49, alpha: 0 },
      },
    })
      .composite([
        {
          input: winnerImageData,
          top: borderWidth,
          left: borderWidth,
        },
        {
          input: loserImageData,
          top: borderWidth,
          left: winnerMetadata.width + borderWidth + spacerWidth,
        },
        {
          input: vsImageData,
          top: Math.round((canvasHeight - vsMetadata.height) / 2),
          left: Math.round(
            winnerMetadata.width + borderWidth + spacerWidth / 2 - vsMetadata.width / 2
          ),
          blend: "screen",
        },
      ])
      .flatten({ background: { r: 43, g: 45, b: 49, alpha: 1 } })
      .webp()
      .toBuffer();

    return combinedImages;
  } catch (error) {
    console.error("Error combining images:", error);
    throw error;
  }
}