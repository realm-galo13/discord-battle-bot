import fs from 'fs';
import path from 'path';
import { TRAIT_NAMES } from '../../config/config.js';
import { getTraitMasteryData } from '../updateStatisticsMemory.js';

const traitMasteryFolderPath = path.join('statistics', 'results', 'traitMastery');
const traitFiles = Object.fromEntries(
  Object.entries(TRAIT_NAMES).map(([key, value]) => [value, `${value}Mastery.json`])
);

export function checkTraitMasterHighlight(battle, options) {
  const winner = battle.getWinner();

  // Get the traits won by the winner
  const winnerTraitsWon = battle.contenderRollsData.filter(
    (data) => data.winnerAddr === winner.address && data.winnerId === winner.id
  );

  for (const traitData of winnerTraitsWon) {
    const traitId = traitData.traitId;
    const traitName = TRAIT_NAMES[traitId];
    //const masteryData = JSON.parse(fs.readFileSync(path.join(traitMasteryFolderPath, traitFiles[traitName]), 'utf-8'));
    const masteryData = getTraitMasteryData(traitName);
    const topPercentIndex = Math.floor(masteryData.length * options.topPercent / 100);

    const winnerMastery = masteryData.find((data) => data.adventurer === `${winner.address}-${winner.id}`);

    if (winnerMastery && masteryData.indexOf(winnerMastery) <= topPercentIndex && winnerMastery.battles >= options.minBattleCount) {
      return {
        adventurer: {
          address: winner.address,
          id: winner.id,
        },
        trait: {
          name: traitName,
          wins: winnerMastery[traitName].wins,
          losses: winnerMastery[traitName].losses,
        }
      };
    }
  }

  return false;
}