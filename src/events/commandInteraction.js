import setChannelCommand from '../commands/setchannel.js';
import getChannelCommand from '../commands/getchannel.js';
import startMonitoringCommand from '../commands/startmonitoring.js';
import stopMonitoringCommand from '../commands/stopmonitoring.js';
import toggleHighlightCommand from '../commands/toggleHighlight.js';
import updateHighlightCommand from '../commands/updateHighlight.js';

export default function (client, state, contract) {
  client.on('interactionCreate', async (interaction) => {
    if (!interaction.isCommand()) return;
  
    const { commandName } = interaction;
  
    if (commandName === 'setchannel') {
      const success = await setChannelCommand.execute(interaction, state);
      if (!success) {
        await interaction.reply('Invalid channel selected. Please choose a text channel.');
      }
    } else if (commandName === 'getchannel') {
      await getChannelCommand.execute(interaction, state);
    } else if (commandName === 'startmonitoring') {
      await startMonitoringCommand.execute(interaction, state, contract);
    } else if (commandName === 'stopmonitoring') {
      await stopMonitoringCommand.execute(interaction, state);
    } else if (commandName === 'togglehighlight') {
      await toggleHighlightCommand.execute(interaction);
    } else if (commandName === 'updatehighlight') {
      await updateHighlightCommand.execute(interaction);
    }
  });
}