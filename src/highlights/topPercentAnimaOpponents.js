import fs from 'fs';
import path from 'path';
import { getAverageAnimaWonData } from '../updateStatisticsMemory.js';

const averageAnimaWonFilePath = path.join('statistics', 'results', 'anima', 'average-adventurer-anima-won-as-opponent.json');

export function checkTopPercentAnimaOpponentsHighlight(battle, options) {
  const opponentAnimaWon = battle.getOpponentAnima();
  //const averageAnimaWonData = JSON.parse(fs.readFileSync(averageAnimaWonFilePath, 'utf-8'));
  const averageAnimaWonData = getAverageAnimaWonData('opponent');

  // Calculate the top topPercent% index
  const topPercentIndex = Math.floor(averageAnimaWonData.length * options.topPercent / 100);

  // Get the threshold from the sorted data
  const topPercentThreshold = parseFloat((averageAnimaWonData[topPercentIndex][1]).averageAnima);

  if (opponentAnimaWon >= topPercentThreshold) {
    return opponentAnimaWon;
  }

  return false;
}