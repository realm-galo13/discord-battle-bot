import fs from 'fs';
import path from 'path';
import { getLossStreaksData } from '../updateStatisticsMemory.js';

const lossStreakFilePath = path.join('statistics', 'results', 'streaks', 'loss-streaks.json');

export function checkLossStreakHighlight(battle, options) {
  const loser = battle.getLoser();
  const loserKey = `${loser.address}-${loser.id}`;
  //const lossStreaks = JSON.parse(fs.readFileSync(lossStreakFilePath, 'utf-8'));
  const lossStreaks = getLossStreaksData();

  for (const [adventurerKey, lossStreak] of lossStreaks) {
    if (adventurerKey === loserKey && lossStreak >= options.minLossStreak) {
      return (lossStreak + 1);
    }
  }

  return false;
}