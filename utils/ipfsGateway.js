import { IPFS_GATEWAYS } from '../config/config.js';

var currentIPFSGatewayIndex = 0;

export function getNextIPFSGateway() {
  const gateway = IPFS_GATEWAYS[currentIPFSGatewayIndex];
  currentIPFSGatewayIndex = (currentIPFSGatewayIndex + 1) % IPFS_GATEWAYS.length;
  return gateway;
}