import { SlashCommandBuilder } from '@discordjs/builders';

const getchannel = {
  data: new SlashCommandBuilder()
    .setName('getchannel')
    .setDescription('Get the current channel set for receiving battle highlight reports'),
  execute: async (interaction, state) => {
    const channel = interaction.guild.channels.cache.get(state.targetChannelId);
    if (channel) {
      await interaction.reply(`The current channel for receiving battle highlight reports is: ${channel.name}`);
    } else {
      await interaction.reply('No channel has been set for receiving battle highlight reports.');
    }
  },
};

export default getchannel;