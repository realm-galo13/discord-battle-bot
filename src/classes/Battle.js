import { divideByDecimalFactor } from '../../utils/helpers.js';
import { TRAIT_NAMES, UPSET_WIN_THRESHOLD } from '../../config/config.js';

export class Battle {
  constructor(transactionHash, fightId) {
    this.transactionHash = transactionHash;
    this.fightId = fightId;
    this.fightStartedData = null;
    this.contenderRollsData = [];
    this.overallWinnerData = null;
  }

  setFightStartedData(fightStartedData) {
    this.fightStartedData = fightStartedData;
  }

  addContenderRollData(contenderRollData) {
    this.contenderRollsData.push(contenderRollData);
  }

  setOverallWinnerData(overallWinnerData) {
    this.overallWinnerData = overallWinnerData;
  }

  isComplete() {
    return this.contenderRollsData.length === 6
    && this.overallWinnerData !== null
    && this.fightStartedData !== null;
  }

  getKey() {
    return `${this.transactionHash}-${this.fightId}`;
  }

  getWinner() {
    if (!this.isComplete()) {
      throw new Error("Cannot calculate opponent score before the battle is complete.");
    }
    const winnerAddress = this.overallWinnerData.addr;
    const winnerId = this.overallWinnerData.adventureId;

    return {
      address: winnerAddress,
      id: winnerId,
    };
  }

  getLoser() {
    if (!this.isComplete()) {
      throw new Error("Cannot calculate opponent score before the battle is complete.");
    }
    const winner = this.getWinner();
    const loserAddress =
      winner.address === this.fightStartedData.attackerAddr
        ? this.fightStartedData.opponentAddr
        : this.fightStartedData.attackerAddr;

    const loserId =
      winner.id === this.fightStartedData.attackerId
        ? this.fightStartedData.opponentId
        : this.fightStartedData.attackerId;

    return {
      address: loserAddress,
      id: loserId,
    };
  }

  getAttackerScore() {
    if (!this.isComplete()) {
      throw new Error("Cannot calculate attacker score before the battle is complete.");
    }
    return (
      this.contenderRollsData.filter(
        (data) =>
          data.winnerAddr === this.fightStartedData.attackerAddr &&
          data.winnerId === this.fightStartedData.attackerId
      ).length ?? 0
    );
  }
    
  getOpponentScore() {
    if (!this.isComplete()) {
      throw new Error("Cannot calculate opponent score before the battle is complete.");
    }
    return (
      this.contenderRollsData.filter(
        (data) =>
          data.winnerAddr === this.fightStartedData.opponentAddr &&
          data.winnerId === this.fightStartedData.opponentId
      ).length ?? 0
    );
  }
    
  getTraitsWonByAttacker() {
    if (!this.isComplete()) {
      throw new Error("Cannot calculate traits won by attacker before the battle is complete.");
    }
  
    const attackerWins = this.contenderRollsData
      .filter(
        (data) =>
          data.winnerAddr === this.fightStartedData.attackerAddr &&
          data.winnerId === this.fightStartedData.attackerId
      );
  
    if (attackerWins.length === 0) {
      return 'none';
    }
  
    return attackerWins
      .map((data) => `${TRAIT_NAMES[data.traitId]} (${data.probability}%)`)
      .join(', ');
  }
      
  getTraitsWonByOpponent() {
    if (!this.isComplete()) {
      throw new Error("Cannot calculate traits won by opponent before the battle is complete.");
    }
  
    const opponentWins = this.contenderRollsData
      .filter(
        (data) =>
          data.winnerAddr === this.fightStartedData.opponentAddr &&
          data.winnerId === this.fightStartedData.opponentId
      );
  
    if (opponentWins.length === 0) {
      return 'none';
    }
  
    return opponentWins
      .map((data) => `${TRAIT_NAMES[data.traitId]} (${data.probability}%)`)
      .join(', ');
  }
      
  getAttackerAnima() {
    if (!this.isComplete()) {
      throw new Error("Cannot calculate attacker anima before the battle is complete.");
    }
    const attackerAddress = this.fightStartedData.attackerAddr;
    const attackerId = this.fightStartedData.attackerId;
  
    const contenderAnima = this.contenderRollsData.reduce((total, data) => {
      const reward = BigInt(data.animaReward) + BigInt(data.animaBonus);
      return total + (data.winnerAddr === attackerAddress && data.winnerId === attackerId ? reward : BigInt(0)) + BigInt(data.attackerReward);
    }, BigInt(0));
  
    const overallAnima = this.overallWinnerData.addr === attackerAddress && this.overallWinnerData.adventureId === attackerId ? BigInt(this.overallWinnerData.anima) : BigInt(0);
  
    const totalAnima = Number(contenderAnima + overallAnima) ?? 0n;
    return divideByDecimalFactor(totalAnima, 18);
  }
  
  getOpponentAnima() {
    if (!this.isComplete()) {
      throw new Error("Cannot calculate opponent anima before the battle is complete.");
    }
    const opponentAddress = this.fightStartedData.opponentAddr;
    const opponentId = this.fightStartedData.opponentId;
  
    const contenderAnima = this.contenderRollsData.reduce((total, data) => {
      const reward = BigInt(data.animaReward) + BigInt(data.animaBonus);
      return total + (data.winnerAddr === opponentAddress && data.winnerId === opponentId ? reward : BigInt(0));
    }, BigInt(0));
  
    const overallAnima = this.overallWinnerData.addr === opponentAddress && this.overallWinnerData.adventureId === opponentId ? BigInt(this.overallWinnerData.anima) : BigInt(0);
  
    const totalAnima = Number(contenderAnima + overallAnima) ?? 0n;
    return divideByDecimalFactor(totalAnima, 18);
  }

  getWinnerLowOddWins(lowOddThreshold) {
    if (!this.isComplete()) {
      throw new Error("Cannot calculate low odd wins before the battle is complete");
    }
    const winner = this.getWinner();
    var lowOddWins = 0;
    for (const roll of this.contenderRollsData) {
      if (roll.winnerId === winner.id && roll.probability <= lowOddThreshold) {
        lowOddWins++;
      }
    }
    return lowOddWins;
  }

  getWinnerUpsetTraitWins() {
    if (!this.isComplete()) {
      throw new Error("Cannot calculate upset trait wins before the battle is complete.");
    }
    const winner = this.getWinner();
    return this.contenderRollsData.filter(
      (data) =>
        data.winnerAddr === winner.address &&
        data.winnerId === winner.id &&
        data.probability < UPSET_WIN_THRESHOLD
    ).length;
  }
}