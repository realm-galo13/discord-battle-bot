# Realm Battle Highlights Bot

A Discord Bot for monitoring and showcasing Realm's battle highlights and statistics. The bot provides various commands for users to view highlights, configure the bot's highlight settings, and fetch battle statistics.

## Developed by

Lucas Franca (Twitter @galo13eth)

## Getting Started

These instructions will guide you through the setup and execution of the project.

### Prerequisites

- Node.js (v16.6.0 or higher)
- Yarn package manager

### Installation

1. Clone the repository and navigate to the project directory:

git clone <repository_url>
cd <project_directory>

2. Install the dependencies:

yarn install

### Usage

#### Fetch Events and Calculate Statistics

Execute the `stats.js` file to fetch events from the contract. If the data directory is empty, it fetches all events; otherwise, it fetches only newer ones.

yarn node stats.js

This command saves all event data to the `data` directory in files with the naming pattern `events{startBlock}_{endBlock}.json`. Afterward, it calculates and updates all statistics, saving them to the `statistics/results` folder.

#### Run the Discord Bot

Execute the `index.js` file to run the Discord Bot:

yarn node index.js

#### Deploy and Update Slash Commands

Execute the `utils/deploy-commands.js` file to deploy and update slash commands for the Discord Bot:

yarn node utils/deploy-commands.js

## Discord Commands

The bot provides several slash commands for users to interact with the bot:

- `/getchannel`: Get the current channel set for receiving battle highlight reports.
- `/setchannel`: Set the channel for receiving battle highlight reports.
- `/startmonitoring`: Start monitoring events and send battle reports to the target channel.
- `/stopmonitoring`: Stop monitoring events and sending battle reports to the target channel.
- `/togglehighlight`: Enable or disable a highlight from being shown by the bot.
- `/updatehighlight`: Update a highlight option.

## Battle Highlights

The bot can generate reports for various battle highlights, which are configurable through the `highlightOptions.js` file or through the `/togglehighlight` and `/updatehighlight` commands. The available highlights and their parameters are:

1. Win Streak: Number of consecutive wins by an adventurer.
   - minWinStreak (default: 8)
   - enabled (default: true)

2. Loss Streak: Number of consecutive losses by an adventurer.
   - minLossStreak (default: 5)
   - enabled (default: true)

3. Top Percent Anima Attackers: Adventurers who have won an amount of Anima that puts them in the top percentage of Anima earners, as attackers.
   - topPercent (default: 5)
   - enabled (default: true)

4. Top Percent Anima Opponents: Adventurers who have won an amount of Anima that puts them in the top percentage of Anima earners, as opponents.
   - topPercent (default: 5)
   - enabled (default: true)

5. Low Odd Wins: Number of trait battle wins in one battle, with a low probability of winning.
   - minLowOddWins (default: 2)
   - lowOddThreshold (default: 10)
   - enabled (default: true)

6. Flawless Victory: Battles where the winner wins all 6 trait battles.
   - enabled (default: true)

7. Upset Wins: Number of trait wins in the same battle, against an opponent with a higher trait value.
   - minUpsetWins (default: 3)
   - enabled (default: true)

8. Rivalry Battle: Number of encounters between two adventurers.
   - minEncounters (default: 4)
   - enabled (default: true)

9. Trait Master: Battles where the winner is in the top percentage of trait mastery.
   - topPercent (default: 3)
   - minBattleCount (default: 25)
   - enabled (default: true)

## License

This project is licensed under the MIT License.