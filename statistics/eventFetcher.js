import { CHUNK_SIZE } from '../config/config.js';
import { createChunks, saveParsedEventsToDisk, readSingleFile } from '../utils/helpers.js';
import { mkdirp } from 'mkdirp';
import fs from 'fs';
import path from 'path';

async function fetchLogsInChunks(contract, eventType, filter, startBlock, endBlock) {
  const logs = [];

  for (let i = startBlock, chunkNum = 1; i <= endBlock; i += CHUNK_SIZE, chunkNum++) {
    const fromBlock = i;
    const toBlock = Math.min(i + CHUNK_SIZE - 1, endBlock);
    const chunkLogs = await contract.queryFilter(filter, fromBlock, toBlock);
    logs.push(...chunkLogs);

    console.log(`[${eventType}] Fetching logs from block ${fromBlock} to ${toBlock}. Logs count: ${logs.length}`);
  }

  return logs;
}

export async function fetchEventsForFileChunks(contract, fileChunks) {
  const eventsFightStarted = [];
  const eventsContenderRoll = [];
  const eventsOverallWinner = [];

  for (const chunk of fileChunks) {
    const filterFightStarted = contract.contractInstance.filters.FightStarted();
    const filterContenderRoll = contract.contractInstance.filters.ContenderRoll();
    const filterOverallWinner = contract.contractInstance.filters.OverallWinner();

    const fightStartedLogs = await fetchLogsInChunks(contract.contractInstance, 'FightStarted', filterFightStarted, chunk.start, chunk.end);
    const contenderRollLogs = await fetchLogsInChunks(contract.contractInstance, 'ContenderRoll', filterContenderRoll, chunk.start, chunk.end);
    const overallWinnerLogs = await fetchLogsInChunks(contract.contractInstance, 'OverallWinner', filterOverallWinner, chunk.start, chunk.end);

    eventsFightStarted.push(...fightStartedLogs.map((log) => {
      const parsedLog = contract.contractInstance.interface.parseLog(log);
      return {
        fightId: parsedLog.args.fightId,
        attackerAddr: parsedLog.args.attackerAddr,
        attackerId: parsedLog.args.attackerId,
        opponentAddr: parsedLog.args.opponentAddr,
        opponentId: parsedLog.args.opponentId,
        particleCost: parsedLog.args.particleCost,
        transactionHash: log.transactionHash,
      };
    }));
    eventsContenderRoll.push(...contenderRollLogs.map((log) => {
      const parsedLog = contract.contractInstance.interface.parseLog(log);
      return {
        fightId: parsedLog.args.fightId,
        winnerAddr: parsedLog.args.winnerAddr,
        winnerId: parsedLog.args.winnerId,
        traitId: parsedLog.args.traitId,
        probability: parsedLog.args.probability,
        animaReward: parsedLog.args.animaReward,
        animaBonus: parsedLog.args.animaBonus,
        attackerReward: parsedLog.args.attackerReward,
        transactionHash: log.transactionHash,
      };
    }));
    eventsOverallWinner.push(...overallWinnerLogs.map((log) => {
      const parsedLog = contract.contractInstance.interface.parseLog(log);
      return {
        fightId: parsedLog.args.fightId,
        addr: parsedLog.args.addr,
        adventureId: parsedLog.args.adventureId,
        anima: parsedLog.args.anima,
        transactionHash: log.transactionHash,
      };
    }));
  }

  const overallBlockRange = {
    start: Math.min(...fileChunks.map(chunk => chunk.start)),
    end: Math.max(...fileChunks.map(chunk => chunk.end)),
  };

  await saveParsedEventsToDisk(contract.address, overallBlockRange, eventsFightStarted, eventsContenderRoll, eventsOverallWinner);
}

export async function readEventsFromDisk() {
  const mainDataDir = 'data';
  await mkdirp(mainDataDir);
  const contractDirs = await fs.promises.readdir(mainDataDir);

  const filePaths = [];

  for (const contractDir of contractDirs) {
    const dataDir = path.join(mainDataDir, contractDir);
    const files = await fs.promises.readdir(dataDir);
    const fullFilePaths = files.map(file => ({ filepath: path.join(dataDir, file), filename: file }));
    filePaths.push(...fullFilePaths);
  }

  const sortedFilePaths = filePaths.sort((a, b) => a.filename.localeCompare(b.filename));

  const allEvents = {
    blockRange: { start: Infinity, end: -Infinity },
    eventsFightStarted: [],
    eventsContenderRoll: [],
    eventsOverallWinner: [],
  };

  for (const { filepath } of sortedFilePaths) {
    const fileData = await readSingleFile(filepath);

    // Update blockRange with the minimum start block and the maximum end block
    allEvents.blockRange.start = Math.min(allEvents.blockRange.start, fileData.blockRange.start);
    allEvents.blockRange.end = Math.max(allEvents.blockRange.end, fileData.blockRange.end);

    allEvents.eventsFightStarted.push(...fileData.eventsFightStarted);
    allEvents.eventsContenderRoll.push(...fileData.eventsContenderRoll);
    allEvents.eventsOverallWinner.push(...fileData.eventsOverallWinner);
  }
  return allEvents;
}

export async function fetchLogsInRange(contract, start, end, chunkSize, filesChunkSize) {
  console.log(`Fetching logs from block ${start} to ${end}...`);
  const chunks = createChunks(start, end, chunkSize);

  for (let i = 0; i < chunks.length; i += filesChunkSize) {
    const fileChunks = chunks.slice(i, i + filesChunkSize);
    await fetchEventsForFileChunks(contract, fileChunks, end);
  }
}