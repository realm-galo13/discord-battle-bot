import { Battle } from './classes/Battle.js';
import { analyzeCompletedBattle } from './analyze.js';
import { updateStatistics } from './updateStatistics.js';
import { updateStatisticsMemory } from './updateStatisticsMemory.js';

const battles = new Map();

export async function monitorTransactions(contract, state) {
  try {
    contract.on('FightStarted', async (fightId, attackerAddr, attackerId, opponentAddr, opponentId, particleCost, event) => {
      const fightStartedData = { fightId, attackerAddr, attackerId, opponentAddr, opponentId, particleCost };
      await handleBattleData(state, event.log.transactionHash, fightId, fightStartedData, (battle, data) => battle.setFightStartedData(data));
    });
  
    contract.on('ContenderRoll', async (fightId, winnerAddr, winnerId, traitId, probability, animaReward, animaBonus, attackerReward, event) => {
      const contenderRollData = { fightId, winnerAddr, winnerId, traitId, probability, animaReward, animaBonus, attackerReward };
      await handleBattleData(state, event.log.transactionHash, fightId, contenderRollData, (battle, data) => battle.addContenderRollData(data));
    });
  
    contract.on('OverallWinner', async (fightId, addr, adventureId, anima, event) => {
      const overallWinnerData = { fightId, addr, adventureId, anima };
      await handleBattleData(state, event.log.transactionHash, fightId, overallWinnerData, (battle, data) => battle.setOverallWinnerData(data));
    });  
  } catch (error) {
    console.error(error);
  }
}

function getBattleKey(transactionHash, fightId) {
  return `${transactionHash}-${fightId}`;
}

async function handleBattleData(state, transactionHash, fightId, battleData, updateFunction) {
  if (!state.monitoring) return;
  try {
    const battleKey = getBattleKey(transactionHash, fightId);
    let battle = battles.get(battleKey);
    if (!battle) {
      battle = new Battle(transactionHash, fightId);
      battles.set(battleKey, battle);
    }
    updateFunction(battle, battleData);
    if (battle.isComplete()) {
      await analyzeCompletedBattle(state, battle);
      updateStatisticsMemory(battle);
      battles.delete(battleKey);
    }  
  } catch (error) {
    console.error(error);
  }
}